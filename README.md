# Num PDE

This repository contains solutions (or attempts to solve) to problems from Num PDE course.
The project is set up by Group #19.

## Set up

As any python project it is best to be run in a virtual environment. 
This is a good practice, which prevents rise of hell with clashing module resolutions.

`Miniconda` is a good full environment manager, but native `venv` can be used equally.

For `conda`:
```
$ conda create -n pde python=3.8
$ conda activate pde
```

For `venv`:
```
$ python3 -m venv pde
$ source pde/bin/activate
```

Once the environment is **set and activated**, install the dependecies:
- numpy
- matplotlib
```
$ pip3 install numpy matplotlib
```

Finally to get the solution - run the `main.py` script:
```
$ python3 main.py --hw <desired hw>
i.e.
$ python3 main.py --hw hw1 // will run the 1st homework
```