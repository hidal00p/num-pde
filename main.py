import argparse as ap
from solution.hw1 import HW1
from solution import Solution


SOLUTIONS = {
    "hw1": HW1(n = 75, m = 75)
}

class SolutionRunner:
    def run(solution: Solution):
        if not isinstance(solution, Solution):
            raise f'Provided solution must inherit from {Solution.__class__}'
        solution.solve()

def main(hw):
    SolutionRunner.run(
        solution=SOLUTIONS[hw]
    )

if __name__ == "__main__":
    parser = ap.ArgumentParser(description="Utility that runs solutions to homeworks from Num PDE class")
    parser.add_argument("--hw", type=str, choices=SOLUTIONS.keys(), help="Select a homework id to run", required=True)
    
    main(
        **vars(parser.parse_args())
    )