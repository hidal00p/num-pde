\section{Exercise 1}

A system of hyperpolic equations is provided, which may describe the conservation
of two coupled properties $u_{1}$ and $u_{2}$.

\begin{align*}
    u = \begin{pmatrix}
        u_{1} \\
        u_{2}
   \end{pmatrix} 
\end{align*}

The coupling is infered from the Jacobian matrix, which reads that the temporal
change of one variable depends linearly on the spatial change of the other.

\begin{align*}
    A = \begin{pmatrix}
       0 && -1 \\
       -1 && 0 
    \end{pmatrix}
\end{align*}

The full system of equation is described as follows:

\begin{align*}
    \begin{cases}
        u_{t} + Au_{x} = 0 \\
        u(x, 0) = u_{0}(x)
    \end{cases}
\end{align*}

Such that:

\begin{align*}
    u_{0}(x) = 
    \begin{cases}
        u_{1}(x, t = 0) = \begin{cases}
           1, \,\, x < 0 \\
           0, \,\, x > 0 
        \end{cases} \\
        u_{2}(x, t = 0) = \begin{cases}
           0, \,\, x < 0 \\
           -2, \,\, x > 0 
        \end{cases}
    \end{cases}
\end{align*}

It is more complex to seek characteristics for the coupled system, and therefore it
is more desirable to find coordinate transform, in such a way that Jacobian matrix is
diagonalized.

Essentially granting the above condition is equivalent to solving the eigenvalue problem.

\begin{align*}
    & u = Tw, \,\, | \,\, w_{t} + Dw_{x} = 0, \\
    & \text{where $D$ is diagonal.}
\end{align*}

Substituting the above condition into the original equation results in the following form:

\begin{align*}
    & Tw_{t} + ATw_{x} = 0 \\
    & w_{t} + T^{-1}ATw_{x} = 0 \\
    & w_{t} + Dw_{x} = 0 \\
    \therefore \,\, &T^{-1}AT = D \\
    \therefore \,\, &AT = TD
\end{align*}

Thus eigenvalue problem.
The characteristic polynomial of the $A$ matrix is the following:

\begin{align*}
    & \lambda^{2} - 1 = 0 \\
    \therefore \,\, & \lambda = \pm 1 \rightarrow \lambda_{1} = 1, \,\, \lambda_{2} = -1
\end{align*}

Solving for eigenvectors yields:

\begin{align*}
    \begin{cases}
        t_{1} = \begin{pmatrix}
            1 \\
            1
        \end{pmatrix} \\
        t_{2} = \begin{pmatrix}
            1 \\
            -1
        \end{pmatrix}
    \end{cases}
\end{align*}

Thus defining a $T$ matrix:

\begin{align*}
    & T = \begin{pmatrix}
        t_{1} && t_{2}
    \end{pmatrix} \rightarrow \\
    & T = \begin{pmatrix}
        1 && 1 \\
        1 && -1
    \end{pmatrix} \\
\end{align*}

As well as $T^{-1}$ matrix:

\begin{align*}
    & T^{-1} = -\frac{1}{2}
    \begin{pmatrix}
        -1 && -1 \\
        -1 && 1
    \end{pmatrix} \rightarrow \\
    & T^{-1} = \frac{1}{2}
    \begin{pmatrix}
        1 && 1 \\
        1 && -1
    \end{pmatrix} \rightarrow \\
    & T^{-1} = \frac{1}{2} T
\end{align*}

And finally the diagonal matrix $D$:

\begin{align*}
    D = \begin{pmatrix}
        1 && 0 \\
        0 && -1
    \end{pmatrix} 
\end{align*}

It can be seen that the transformation matrix consists of eigenvectors of the original
Jacobian matrix. Therefore the system may be reformulated in the following terms:

\begin{align*}
    & w(x, t) = 
    \frac{1}{2}\begin{pmatrix}
       1 && 1 \\
       1 && -1 
    \end{pmatrix} u(x, t) \\
    \therefore \,\, & w_{0}(x) = 
    \frac{1}{2}\begin{pmatrix}
       1 && 1 \\
       1 && -1 
    \end{pmatrix} u_{0}(x)
\end{align*}

Due to the fact that the hyperbolic system is decoupled in the new system of coordinates,
the solutions to the new system are the solution to the advection problem with
constant propagation speed:

\begin{align*}
    & w(x, t) = 
    \begin{pmatrix}
        w_{1}(x, t) \\
        w_{2}(x, t)
    \end{pmatrix} =
    \begin{pmatrix}
          w_{1}(x - t, t = 0) \\
          w_{2}(x + t, t = 0)
    \end{pmatrix} \\
    & w(x, t) = 
    \begin{pmatrix}
        w_{1}(x, t) = 
        \begin{cases}
            0.5, \,\, x < t \\
            -1, \,\, x > t
        \end{cases} \\
        w_{2}(x, t) = 
        \begin{cases}
            0.5, \,\, x < -t \\
            1, \,\, x > -t
        \end{cases}
    \end{pmatrix}
\end{align*}

Transforming back to the original results will result in a certain superposition of the above solutions.

\begin{align*}
    & u(x, t) = Tw(x, t) \\
    \therefore \,\,
    & u(x, t) = 
    \begin{pmatrix}
       1 && 1 \\
       1 && -1 
    \end{pmatrix} \begin{pmatrix}
        w_{1}(x, t) \\
        w_{2}(x, t)
    \end{pmatrix} \\
    = & \begin{pmatrix}
        w_{1}(x, t) + w_{2}(x, t) \\
        w_{1}(x, t) - w_{2}(x, t) \\
    \end{pmatrix} \\
    = & \begin{cases}
        u_{1}(x ,t) = 
        \begin{cases}
            1, \,\, x < -t \\
            1.5, \,\,  -t < x < t \\
            0, \,\, x > t
        \end{cases} \\
        u_{2}(x ,t) = 
        \begin{cases}
            0, \,\, x < -t \\
            -0.5, \,\,  -t < x < t \\
            -2, \,\, x > t
        \end{cases}
    \end{cases}
\end{align*}
