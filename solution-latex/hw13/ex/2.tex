\section{Exercise 2}
Show that the scalar \textit{viscous Burgers} equation

$$ u_t + u u_x = \varepsilon u_{xx} $$

with initial conditions given as follows

$$ u\left(x,t=0\right)=u_0\left(x\right)=\begin{cases}u_L&x<0\\ u_R&x>0\end{cases},\:\:\mathrm{where}\:\:u_L>u_R  $$

has a travelling wave solution of the form

$$ u(x,t) = w(y) \:\:\mathrm{with}\:\: y:= x - s t, $$

by taking the following steps:

\begin{enumerate}
    \item Derive an ordinary differential equation (ODE) for $w$ and verify that this ODE has solutions of the form

    $$ w\left(y\right)=u_R\:+\frac{1}{2}\left(u_L+u_R\right)\left[1-\tanh \left(\frac{\left(u_L-u_R\right)y}{4\:\varepsilon }\right)\right]  $$
    
    \vspace{0.05cm}
    
    \item Show that the shock speed is

    $$ s = \dfrac{u_L + u_R}{2} $$

    so the shock speeds of the \textit{inviscid Burger’} equation and the \textit{viscous Burgers’} equation are the same.
    
    \item What happens to the solution when $\varepsilon \to 0 $?

    \item Plot the solution $w(y)$ for the case when $u_L = 1$ and $u_R = 0$ together with

    $$ \varepsilon = \{ 0.2, 0.1, 0.01 \} $$
\end{enumerate}

\subsection{Solution:}

\textbf{(a)} Considering the given

$$ u(x,t) = w(y) = w(x-st)  $$

taking the partial derivatives of $u(x,t)$ with respect to $x$ and $t$

$$ \begin{cases}u_t=-sw'&\\ u_x=w'&\end{cases},\:\:\:\:\:\:\mathrm{and}\:\:\:\:\:\:\:u_{xx}=w'' $$

\vspace{0.4cm}

Now, one can substitute the partial derivatives into the \textit{viscous Burgers'} equation

$$ -sw' + w w' = \varepsilon w'' $$

\vspace{0.2cm}

The equation can be rewritten and integrated on both sides 

$$ -s w' + \left(\dfrac{w^2}{2}\right)' = \varepsilon w'' \:\: \Rightarrow \:\: -sw + \dfrac{w^2}{2}=\varepsilon w' + C_1 $$


\vspace{0.3cm}

Additionally, one can apply the boundary conditions on the function $w$, considering the limits given as hint:

$$ \lim _{y\to -\infty }\:w\left(y\right)=u_L\:,\:\:\lim _{y\to +\infty \:}\:w\left(y\right)=u_R\:,\:\:\lim _{y\to \pm \infty \:}\:w'\left(y\right)=0\:\:\: $$

\vspace{0.4cm}

I. For $ x < 0 \Leftrightarrow y < st: u(x,t=0)=w(y)=u_L \:\: \Rightarrow \:\: C_1 = -s u_L + \dfrac{u^2_L}{2}$

II. For $ x > 0 \Leftrightarrow y > st: u(x,t=0)=w(y)=u_L \:\: \Rightarrow \:\: C_1 = -s u_R + \dfrac{u^2_R}{2}$

\vspace{0.4cm}

By comparing both equations for $C_1$, one can find the following

$$ -s u_L + \dfrac{u^2_L}{2} = -s u_R + \dfrac{u^2_R}{2} \: \Leftrightarrow \: s = \dfrac{u_L + u_R}{2} $$

\vspace{0.5cm}

This result is also the expression for the shock speed of the \textit{inviscid Burgers'} equation.

Thus, this is also the answer for item (b)

$$ s = \dfrac{u_L + u_R}{2} $$

\vspace{0.2cm}

Additionally, the constant $C_1$ can be computed as

$$ C_1 = -s u_L + \dfrac{u^2_L}{2} = - \dfrac{u_L + u_R}{2} u_L + \dfrac{u^2_L}{2} = - \dfrac{u_L u_R}{2}$$


\vspace{0.2cm}

Now, by making use of the method of separation of variables into the establish ODE (the integrated one), one can obtain 

$$ -\left( \dfrac{u_L + u_R}{2} \right)w + \dfrac{w^2}{2} = \varepsilon \dfrac{dw}{dy} - \dfrac{u_L u_R}{2} $$

$$ \Leftrightarrow \:\:\: \dfrac{dy}{2\varepsilon} = \dfrac{dw}{ \left(w - \dfrac{u_L + u_R}{2} \right)^2 - \left(\dfrac{u_L - u_R}{2}\right)^2 } $$

\vspace{0.4cm}

By considering the given hint and the relation $u_L > w > u_R$, one can obtain by integration

$$ \dfrac{y}{2\varepsilon} + C_2 = \dfrac{1}{u_L - u_R} \log \left( \dfrac{u_L -w}{w - u_R} \right)$$

\vspace{0.2cm}

By solving for $w$

$$ w(y) = u_R + \left( \dfrac{u_L - u_R}{2} \right) \dfrac{2}{e^{A(y)} + 1} \:\:\: \mathrm{where} \:\:\: A(y) := \dfrac{(u_L - u_R)y}{2\varepsilon} + C_2$$

\vspace{0.4cm}

Now, by multiplying the numerator and denominator by $e^{-\frac{A(y)}{2}}$

$$ w(y) = u_R + \left( \dfrac{u_L - u_R}{2} \right) \dfrac{2 e^{-\frac{A(y)}{2}}}{e^{+\frac{A(y)}{2}} + e^{-\frac{A(y)}{2}}} = u_R + \dfrac{u_L - u_R}{2} \left( 1 - \dfrac{e^{+\frac{A(y)}{2}} - e^{-\frac{A(y)}{2}}}{e^{+\frac{A(y)}{2}} + e^{-\frac{A(y)}{2}}}  \right) = $$

$$ = u_R + \dfrac{u_L - u_R}{2} \left( 1 - \tanh \left( \dfrac{A(y)}{2} \right)  \right) $$

\vspace{0.4cm}

By substituting $A(y)$ one arrives to the solution of the \textit{viscous Burgers'} equation

$$ w(y) = u_R + \dfrac{u_L - u_R}{2} \left( 1 - \tanh \left( \dfrac{ (u_L - u_R)y}{4\varepsilon} + \dfrac{C_2}{2} \right)  \right) $$

\vspace{0.5cm}

\textbf{(b)} The expression for the Shock Speed for the \textit{viscous Burgers'} equation $ \dfrac{u_L + u_R}{2} $ was developed in item (a) when comparing the equation for $C_1$

\vspace{0.4cm}

\textbf{(c)} As shown in the plot of the item (d), when $\varepsilon \to \infty$, the solution tends to the step function.

\vspace{0.4cm}

\textbf{(d)} Considering the given values $u_L = 1$, $u_R=0$, $\varepsilon = \{ 0.2, 0.1, 0.01 \}$ and the range $y \in [-1,1]$, one can plot the solution $w(y)$ to the \textit{viscous Burgers'} equation

(Additionally, one considers $C_2 = 0$)

\vspace{0.3cm}

\begin{center}
    
\begin{tikzpicture}[scale=1.2]
\begin{axis}[
    xlabel=$y$,
    ylabel=$w(y)$,
    xmin=-1, xmax=1,
    ymin=0, ymax=1,
    grid=both,
    grid style={line width=.1pt, draw=gray!20}
]
\addplot[red,domain=-1:1,samples=100] {0 + ((1+0)/2)*( 1 - tanh( (((1-0)*x)/(4*0.01))+ 0) )};
\addlegendentry{$\varepsilon = 0.01$}
\addplot[blue,domain=-1:1,samples=100] {0 + ((1+0)/2)*( 1 - tanh( (((1-0)*x)/(4*0.05))+ 0) )};
\addlegendentry{$\varepsilon = 0.05$}
\addplot[black,domain=-1:1,samples=100] {0 + ((1+0)/2)*( 1 - tanh( (((1-0)*x)/(4*0.1))+ 0) )};
\addlegendentry{$\varepsilon = 0.10$}
\addplot[green,domain=-1:1,samples=100] {0 + ((1+0)/2)*( 1 - tanh( (((1-0)*x)/(4*0.15))+ 0) )};
\addlegendentry{$\varepsilon = 0.15$}
\addplot[orange,domain=-1:1,samples=100] {0 + ((1+0)/2)*( 1 - tanh( (((1-0)*x)/(4*0.2))+ 0) )};
\addlegendentry{$\varepsilon = 0.20$}
\end{axis}
\end{tikzpicture}
\end{center}

\vspace{0.3cm}