\documentclass[12pt, a4paper, titlepage]{article}

% for the schedule section
\usepackage[table,xcdraw]{xcolor}

%% Language and font encodings
\usepackage[english]{babel}
\usepackage[utf8]{inputenc} %% it was needed to change from 'utf8x' to 'utf8'
\usepackage[T1]{fontenc}

%% Sets page size and margins
\usepackage[a4paper,top=3cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}
\usepackage{indentfirst}

%% Useful packages
\usepackage{caption}
\usepackage[font=footnotesize,labelformat=simple]{subcaption}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{subcaption}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{varioref} 
\usepackage[colorlinks=true, allcolors=blue]{hyperref}
\usepackage{cleveref}
\usepackage{blkarray}
\usepackage{lipsum}
\usepackage{textcomp}
\usepackage{gensymb} 
\usepackage{titlesec}
\usepackage{titlepic}
\usepackage{amsmath,amsthm,array}
\usepackage{longtable}
\usepackage{subfigure}
\usepackage{appendix}
\renewcommand\appendixpagename{Attachments}
\usepackage{pdfpages}
\usepackage{tikz}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{vwcol}  
\usepackage{setspace}
\usepackage{colortbl}
\usepackage{hhline}
\usetikzlibrary{arrows,decorations.pathmorphing,backgrounds,positioning,fit,petri}
\usepackage{listings}
\usepackage{comment}
\usepackage{enumerate}   

\title{}

\usepackage{titlesec}
\usepackage{hyperref}
\usepackage{xcolor}
\usepackage{listings}
\lstset{basicstyle=\ttfamily,
  showstringspaces=false,
  commentstyle=\color{red},
  keywordstyle=\color{blue}
}
\usepackage{amsfonts} 
\usepackage{enumitem}
\usepackage{cancel}

\begin{document}

\begin{onehalfspacing} 

\section{Exercise 2.(Transformation to Reference Element)}

Let $\hat{T}$ be a known reference triangle with vertices $e_1 = (0, 0), e_2 = (1, 0)$ and $e_3 = (0, 1)$. We define an affine linear map $F : \hat{T} \mapsto T, \xi \mapsto B\xi + \Tilde{\xi}$ on any triangle $T \subset R^2$ with vertices $a_1, a_2, a_3$.


For the present exercise, with $C$ we will denote a general constant which is independent of the mesh size and the solution. We might include new factors into $C$ without changing the notation.

\vspace{0.3cm}

Figure 1 (provided in the question statement): Affine mapping from reference triangular to the original mesh.

\vspace{0.3cm}

\begin{enumerate}[label=(\alph*),leftmargin=1.35cm]
    \item Find $||B||^2_{L^2}$ and $||B^{-1}||^2_{L^2}$ in terms of $a_1, a_2, a_3$ and $\theta$.
    \item Assuming shape regularity of the mesh, prove that $$ || u - I_1 u ||_{H^1 (T)} \:\le \:\: C \sqrt{\left|a_2-a_1\right|^2+\left|a_3-a_1\right|^2}\:\left|\left|D_x^2u \right|\right|_{0,T} $$
    \item In the lecture you derived an estimate for the error in $H^1 (\Omega)$ i.e. for $e_1 = ||u - u_h||_{H^1 (\Omega)}$. Let us assume that you perform a convergence analysis in which you study the variation of error in $e_1$ by refining the grid such that $| a_3 - a_1 | $ and $|a_2 - a_1|$ remain the same whereas $\theta$ and $|a_2 - a_3|$ are monotonically decreased. Will your convergence study match the theoretical result obtained in the lecture?
\end{enumerate}

Hint: Use the Bramble-Hilbert Lemma and the lecture notes to bound the interpolation error.

\vspace{0.2cm}

\section{Solution:}

\textbf{(a)} Considering the given notation and the formulas from the lecture notes, one has the transformation

$$  F\:\begin{pmatrix}\xi _1\\ \xi _2\end{pmatrix}\:=\:a_1\:+\:\left(a_2\:-\:a_1,\:a_3\:-\:a_1\right)\:\begin{pmatrix}\xi _1\\ \xi _2\end{pmatrix}= \Tilde{\xi} \:+\:B\:\xi  $$

where 
$$B = \begin{pmatrix}b_{11}&b_{12}\\ b_{21}&b_{22}\end{pmatrix} =\left(a_2\:-\:a_1,\:a_3\:-\:a_1\right)=\left(\:b_1\:,\:b_2\:\right) $$

\vspace{0.2cm}

Additionally, one can define the vectors

$$ b_1 = a_2 - a_1 $$
$$ b_2 = a_3 - a_1 $$

$ \Rightarrow$ For calculating the Frobenius norm of $B$, one can consider $B$ as a flatten matrix, therefore 

$$ ||B||^2_{L^2} = b_{11}^2 + b_{12}^2 + b_{21}^2 + b_{22}^2 $$

\vspace{0.2cm}

One can assume $B$ (as a vector, flatten matrix) as a sum of two vector by applying the triangular inequality  ($|a+b|^2 \le |a|^2 + |b|^2$)

$$ ||B||^2_{L^2} = b_{11}^2 + b_{21}^2 + b_{12}^2 + b_{22}^2 = ||b_1 + b_2||^2_{L^2} \le ||b_1||^2 + ||b_2||^2 = |b_1|^2 + |b_2|^2 = |a_2 - a_1|^2 + |a_3 - a_1|^2 $$

\vspace{0.2cm}

$ \Rightarrow $ The same procedure follows for the calculation of the Frobenius norm of $B^{-1}$ (one can consider $B^{-1}$ as a flatten matrix) 

$$||B^{-1}||^2_{L^2} =\left|\left|\frac{1}{det\left(B\right)}\left[\begin{pmatrix}b_{22}&-b_{12}\\ -b_{21}&b_{11}\end{pmatrix}\right]\right|\right|^2 = \frac{1}{\left|det\left(B\right)\right|^2}\left[b_{22}^2\:+\:b_{12}^2\:+\:b_{21}^2\:+\:b_{11}^2\right] $$

\vspace{0.2cm}

Additionally, one considers the definition of the Cross product 

$$ a \times b = ||a|| \cdot ||b|| sin(\theta) $$

calculated by two vectors (in this case $b_1$ and $b_2$) associated with the triangle $T$ and considering that the Cross product can also be calculated by the determinant of $B$ given by $det(B) = b_{11} b_{22} - b_{12} b_{21}$.

$$||B^{-1}||^2_{L^2} \le \frac{1}{||b_1||^2 \cdot ||b_2||^2 sin^2(\theta)}\left[\:|b_1|^2\:+\:|b_2|^2\right] = \:\frac{1}{sin^2\left(\theta \right)}\left[\:\frac{|b_1|^2\:+\:|b_2|^2\:}{|b_1|^2\:\cdot \:|b_2|^2}\right] = $$

$$ = \frac{1}{sin^2\left(\theta \:\right)}\left[\:\frac{1}{\left|b_2\right|^2}\:+\:\frac{1}{\left|b_1\right|^2}\right] $$

\textbf{(b)} From the lecture notes, one has

$$ \left|\left|u-I_hu\right|\right|^2_{1,T}=\sum _{l=0}^1\:\left|\left|D^l_x\left(u-I_hu\right)\right|\right|_{0,T}^2 = \left|\left|u-I_hu\right|\right|^2_{0,T} + \sum _{|l|=1}\:\left|\left|D^l_x\left(u-I_hu\right)\right|\right|_{0,T}^2$$

Additionally (from the lecture notes), one can apply the transformation

$$ \left|\left|u-I_hu\right|\right|^2_{0,T} \le C \cdot |det(B)| \cdot \left|\left|\hat{u} - I_1 \hat{u}\right|\right|^2_{0,\hat{T}}  $$

$$ \left|\left|D^l_x\left(u-I_hu\right)\right|\right|_{0,T}^2 \le C \cdot ||B^{-1}||^2 \cdot |det(B)| \cdot \left|\left|D^k_{\xi} \left(\hat{u}-I_1 \hat{u}\right)\right|\right|_{0,\hat{T}}^2 , \:\: |k|=|l|=1 $$

\vspace{0.2cm}

$ \Rightarrow $ Using the Bramble-Hilbert Lemma

$$ \left|\left|\hat{u} - I_1 \hat{u}\right|\right|^2_{0,\hat{T}} \le C \cdot \left|\left|D^k_{\xi} \hat{u} \right|\right|_{0,\hat{T}}^2 , \:\: |k|=2$$

$$ \left|\left|D^k_{\xi} \left(\hat{u}-I_1 \hat{u}\right)\right|\right|_{0,\hat{T}}^2 \le C \cdot \left|\left|D^p_{\xi} \hat{u} \right|\right|_{0,\hat{T}}^2 , \:\: |k|=1,|p|=2 $$

\vspace{0.2cm}

Transforming to $x$ domain

$$ |det(B)| \cdot \left|\left|D^p_{\xi} \hat{u} \right|\right|_{0,\hat{T}}^2 \le C \cdot ||B||^4 \cdot \left|\left|D^p_{x} u \right|\right|_{0,T}^2 , \:\:|p|=2$$

\vspace{0.2cm}

$ \Rightarrow $ By combining the inequalities, one obtains

$$ \left|\left|u - I_1 u\right|\right|^2_{0,T} \le C ||B||^4 \left|\left|D^p_{x} u \right|\right|_{0,T}^2 + C ||B^{-1}||^2 ||B||^4 \left|\left|D^p_{x} u \right|\right|_{0,T}^2 = C \left( ||B||^4 + ||B^{-1}||^2 ||B||^4 \right) \left|\left|D^p_{x} u \right|\right|_{0,T}^2  $$
\vspace{0.2cm}

Assuming shape regularity of the mesh $\Rightarrow ||B|| \: ||B^{-1}|| \le C \Rightarrow ||B||^2 ||B^{-1}||^2 \le C$

\vspace{0.2cm}

One can imply 

$$ \left|\left|u - I_1 u\right|\right|^2_{0,T} \le C ||B||^2 \left|\left|D^p_{x} u \right|\right|_{0,T}^2 $$

Therefore,  by replacing $||B||$ obtained from item (a):


$$ \left|\left|u - I_1 u\right|\right|^2_{0,T} \le C \left(\sqrt{\left|a_2-a_1\right|^2+\left|a_3-a_1\right|^2}\right) \left|\left|D^2_{x} u \right|\right|_{0,T}^2 $$

\vspace{0.4cm}

\textbf{(c)} Considering the refinement mentioned in the problem:

\vspace{0.3cm}

Where $|a_3 - a_1|$ and $ |a_3 - a_1|$ remain the same and $\theta$ and $|a_2 - a_3|$ decrease monotonically.
The proposed refinement does not lead to a shape regular mesh, since the volume of the cell approaches zero when $\theta \to 0$, but the diameter does not. 

In the lecture notes the mesh was assumed to be regular $ \Rightarrow $ If the volume of the cell goes to zero then so does the diameter.

\vspace{0.2cm}

$\Rightarrow $ Thus, the convergence with the proposed refinement will not match the theoretical results obtained in the lecture notes.





\end{onehalfspacing}

\end{document}



