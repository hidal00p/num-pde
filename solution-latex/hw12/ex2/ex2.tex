\documentclass[12pt, a4paper, titlepage]{article}

% for the schedule section
\usepackage[table,xcdraw]{xcolor}

%% Language and font encodings
\usepackage[english]{babel}
\usepackage[utf8]{inputenc} %% it was needed to change from 'utf8x' to 'utf8'
\usepackage[T1]{fontenc}

%% Sets page size and margins
\usepackage[a4paper,top=3cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}
\usepackage{indentfirst}

%% Useful packages
\usepackage{caption}
%\usepackage[font=footnotesize,labelformat=simple]{subcaption}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
%\usepackage{subcaption}
%\usepackage[colorinlistoftodos]{todonotes}
%\usepackage{varioref} 
%\usepackage[colorlinks=true, allcolors=blue]{hyperref}
%\usepackage{cleveref}
%\usepackage{blkarray}
%\usepackage{lipsum}
%\usepackage{textcomp}
\usepackage{gensymb} 
\usepackage{titlesec}
\usepackage{titlepic}
\usepackage{amsmath,amsthm,array}
\usepackage{longtable}
\usepackage{subfigure}
\usepackage{appendix}
\renewcommand\appendixpagename{Attachments}
\usepackage{pdfpages}
\usepackage{tikz}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{vwcol}  
\usepackage{setspace}
\usepackage{colortbl}
\usepackage{hhline}
\usetikzlibrary{arrows,decorations.pathmorphing,backgrounds,positioning,fit,petri}
\usepackage{listings}
\usepackage{comment}
\usepackage{enumerate}   
\usetikzlibrary{intersections,backgrounds}
\usepackage{pgfplots}
\usepackage{eucal}

\title{}

\usepackage{titlesec}
\usepackage{hyperref}
\usepackage{xcolor}
\usepackage{listings}
\lstset{basicstyle=\ttfamily,
  showstringspaces=false,
  commentstyle=\color{red},
  keywordstyle=\color{blue}
}
\usepackage{amsfonts} 
\usepackage{enumitem}
\usepackage{cancel}

\pgfplotsset{compat=1.18}

\begin{document}

\begin{onehalfspacing} 

\section{Exercise 2. (Convergence of Upwind scheme)}

The $Upwind$ scheme used for linear advection equation $ u_t + a u_x = 0$, $\forall a > 0$, is given by 

$$ u^{n+1}_i = u^n_i + \dfrac{a \Delta t}{ \Delta x} (u^{n}_{i-1} - u^{n}_{i})  $$

\vspace{0.3cm}



\vspace{0.3cm}


\begin{enumerate}[label=(\alph*),leftmargin=1.35cm]
    \item Use the \textit{Lax equivalence theorem} and the following condition

    $$ 0 \le \dfrac{a\Delta t}{\Delta x} \le 1  $$
    \vspace{0.05cm}
    
    \item We would like to modify the \textit{Upwind} scheme so that it is numerically stable for the case $ a < 0 $. What should be updated for the above proof of part (a)? 
    
    \item Instead of the linear advection equation given above, we would like to consider a linear hyperbolic system. Which conditions do we need, if we apply the \textit{Upwind} schema to solve for the decoupled system of equations? 
    
\end{enumerate}

\vspace{0.2cm}


\section{Solution:}

\textbf{(a)} In order to show convergence of the given \textit{Upwind} scheme, one can use the \textit{Lax equivalence theorem} that establishes 

$$ \mathrm{consistency} + \mathrm{stability} \Leftrightarrow \mathrm{convergence} $$

\vspace{0.4cm}

\textbf{I. Consistency:} in order to show consistency, one must consider the expression

$$ L_{\Delta t}(x,t) \le C \cdot \Delta t $$
\vspace{0.2cm}

Where, from the definition given in the Lecture Notes 

$$ L_{\Delta t}(x,t) = \dfrac{1}{\Delta t} \left[ u(x,t+\Delta t) - u(x,t) - \dfrac{a \Delta t}{\Delta x} (u(x-\Delta x,t) - u(x,t)) \right] $$
\vspace{0.2cm}

Now, making use of the Taylor expansion in space and time, one can calculate


\begin{align*}
    L_{\Delta t}(x,t) & = \dfrac{1}{\Delta t} [ u(x,t) + \Delta t u_t(x,t) + \dfrac{\Delta t^2}{2} u_{tt}(x,t) - u(x,t) +a \Delta t u_x(x,t) \\ 
    & - \dfrac{a \Delta t \Delta x}{2} u_{xx}(x,t)(x,t) + \mathcal{O}(\Delta t^3) ] \\
    & = u_t(x,t) + \dfrac{\Delta t}{2} u_{tt}(x,t) - \dfrac{a\Delta x}{2} u_{xx}(x,t)(x,t) + \mathcal{O}(\Delta t^2) 
\end{align*}

\vspace{0.2cm}

Knowing that the exact solution $u$ of the advection equation, that is
\begin{align*}
   u_t & = -a u_x \\
   u_{tt} & = a^2 u_{xx}
\end{align*}

\vspace{0.1cm}

Therefore, it leads to

\begin{align*}
    L_{\Delta t}(x,t) & = -a u_x (x,t)  +  \dfrac{a^2 \Delta t}{2} u_{xx}(x,t) + a u_x(x,t) - \dfrac{\Delta x}{2} u_{xx} (x,t) + \mathcal{O} (\Delta t^2) \\ & = \dfrac{a u_{xx} (x,t) }{2} \left( a - \dfrac{\Delta x}{\Delta t} \right) \Delta t + \mathcal{O} (\Delta t^2)
\end{align*}

Since, 
$$ \dfrac{\Delta x}{\Delta t} = Const. \:\: \Rightarrow \:\: \mathcal{O}(\Delta t) = \mathcal{O}(\Delta x)$$

Consequentially,

$$ || L_{\Delta t} (\cdot,t) ||_1  = \dfrac{a}{2} \left( a - \dfrac{\Delta x}{ \Delta t} \right) || u_{xx} (\cdot,t)|| \Delta t \le C \cdot \Delta t$$

\vspace{0.5cm}

\textbf{II. stability:} in order to show stability (in $L_1$), it follows 

$$ || u^{n+1}||_1 \le ||u^n||_1$$

\vspace{0.2cm}

One can write the given scheme and compute the $L_1$ norm using triangle inequality

\begin{align*}
    || u^{n+1}||_1 = & \sum _i\:\left|u^{n+1}_i\right|=\sum _i\:\left|\left(1-\frac{a\Delta t}{\Delta x}\right)\cdot u_i^n\:+\frac{a\Delta t}{\Delta x}\cdot u^n_{i-1}\:\right| \\
    \le & \sum _i\left|\left(1-\frac{a\Delta t}{\Delta x}\right)\cdot \:u_i^n\right|+\sum_i\left|\frac{a\Delta t}{\Delta x}\cdot \:u^n_{i-1}\right|
\end{align*}

\vspace{0.2cm}

By considering the given condition $0 \le \frac{a\Delta t}{\Delta x} \le 1$

\begin{align*}
    || u^{n+1}||_1 \le & \left(1-\frac{a\Delta t}{\Delta x}\right) \sum _i |u_i^n |+ \frac{a\Delta t}{\Delta x} \sum_i |u^n_{i-1} | \\ 
    = & \left(1-\frac{a\Delta t}{\Delta x}\right) ||u_i^n ||_1 + \frac{a\Delta t}{\Delta x} ||u^n_{i-1} ||_1 = ||u^n||_1
\end{align*}

\vspace{0.5cm}

\textbf{(b)} In order to modify the the \textit{Upwind} scheme for stability in the case of $a<0$, one must modify the stencils 

$$ u^{n+1}_i = u^n_i + \dfrac{a \Delta t}{ \Delta x} (u^{n}_{i} - u^{n}_{i-1} )  $$

\vspace{0.2cm}

Considering that the arguments for consistency used for item (a) are in the same way applicable, one can compute the $L_1$ norm of the new scheme (also using triangle inequality )

$$ || u^{n+1}||_1 \le \sum _i\left|\left(1 + \frac{a\Delta t}{\Delta x}\right)\cdot \:u_i^n\right|+\sum_i\left| - \frac{a\Delta t}{\Delta x}\cdot \:u^n_{i-1}\right| $$

\vspace{0.4cm}

Now, for the new scheme to have numerically stability, it has to hold for

$$ -1 \le \frac{a\Delta t}{\Delta x} \le 0 $$

So that, 

$$ \left(1 + \frac{a\Delta t}{\Delta x}\right)  \ge 0 \:\:\:\mathrm{and}\:\:\:  - \frac{a\Delta t}{\Delta x}  \ge 0 $$ 
\vspace{0.4cm}

Therefore, under the new conditions, one has

$$ || u^{n+1}||_1 \le ||u^n||_1$$

\vspace{0.5cm}


\textbf{(c)} Considering hyperbolicity, one can diagonalise the matrix $A$ in the equation 

$$ \vec{u_t}\:+\:A\:\vec{u_x}\:=\:0 $$

One can have $n$ separate equations in the form of 

$$ (w_p)_t + \lambda_p (w_p)_x $$
\vspace{0.1cm}

Where $\lambda_p$ are considered the eigenvalues of the matrix $A$
\vspace{0.2cm}

Additionally, if one wants to use the \textit{Upwind} scheme to solve scalar equation, one then must consider that for every $\lambda_p$ the stability condition must be satisfied.
\vspace{0.2cm}

That is

$$ 0 \le \frac{\lambda_p\Delta t}{\Delta x} \le 1 \:\:,\mathrm{ for}\:\:\: p=1,...,n$$


\vspace{0.6cm}


\end{onehalfspacing}

\end{document}



