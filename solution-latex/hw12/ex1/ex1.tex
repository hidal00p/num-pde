\section{Exercise 1}
For the advection equation $u_t + a u_x = 0$ the Lax-Wendroff method
\begin{equation}
    u_i^{n+1} = u_i^{n} + \frac{a \Delta t}{2 \Delta x}\left( u_{i-1}^{n} - u_{i+1}^{n} \right) + \frac{a^2 \Delta t^2}{2 \Delta x^2} \left( u_{i+1}^{n} - 2u_i^{n} + u_{i-1}^{n} \right)
\end{equation}
is used.
Show that in the case of smooth solution and $\Delta t \sim \Delta x$ for the local error $L_{\Delta t}$ 
\begin{equation}
    \norm{L_{\Delta t} \left( \cdot, t \right)} \le C \Delta t^2
\end{equation}
holds and give the constant $C$.

\subsection{Solution}
Using the definition of the local truncation error as in Definition II.12 Consistency gives
\begin{equation}
    L_{\Delta t} \left( x,t \right) 
    = \frac{1}{\Delta t} \left( u \left( x,t + \Delta t \right) \right) 
    - H_{\Delta t}\left( u \left( \cdot,t \right); x \right)
.\end{equation}
Here $u \left( x,t \right)$ is the exact solution to $u_t + a u_x = 0$
Now plugging the Lax-Wendroff Method with the exact solution $u \left( x,t \right)$ in for $H_{\Delta t}$ gives
\begin{multline}
    \label{eq:truncationError}
    L_{\Delta t} \left( x,t \right)
    = \frac{1}{\Delta t}
    \Biggl[ 
        u \left( x,t + \Delta t\right)
        - \Biggl(
            u \left( x,t \right)
            + \frac{a \Delta t}{2 \Delta x}
            \left( 
                u \left( x - \Delta x,t \right) - u \left( x + \Delta x, t \right)
            \right)\\
            + \frac{a^2 \Delta t^2}{2 \Delta x ^2}
            \left( 
                u \left( x + \Delta x, t \right) - 2u \left( x,t \right) + u \left( x-\Delta x,t \right)
            \right)
        \Biggr)
    \Biggr]
.\end{multline}
For the approximation at the points $u\left( x,t+\Delta t \right), u \left( x - \Delta x,t \right)$ and $u \left( x + \Delta x,t \right)$ the Taylor expansions truncated after quadratic terms are used
\begin{align}
    \label{eq:taylorTime}
    u \left( x,t + \Delta t \right) 
    &= u \left(  x,t \right)
    + u_t \left( x,t \right) \Delta t   
    + u_{tt} \left( x,t \right) \frac{\left( \Delta t \right)^2}{2}
    + \mathcal{O}\left( \left( \Delta t \right)^3 \right)\\
    \label{eq:taylorDeltaX}
    u \left( x + \Delta x,t \right)
    &= u \left( x,t \right)
    + u_x \left( x,t \right) \Delta x
    + u_{x x}\left( x,t \right) \frac{\left( \Delta x \right)^2}{2}
    + \mathcal{O}\left( \left( \Delta x \right)^3 \right)\\
    \label{eq:taylorMDeltaX}
    u \left( x - \Delta x,t \right)
    &= u \left( x,t \right)
    - u_x \left( x,t \right) \Delta x
    + u_{x x}\left( x,t \right) \frac{\left( \Delta x \right)^2}{2}
    + \mathcal{O}\left( \left( \Delta x \right)^3 \right)
.\end{align}
Using the problem statement to formulate $u_t = - a u_x$ and plug it into \cref{eq:taylorTime} lets us reformulate as
\begin{equation}
    u \left( x,t \Delta t\right)
    = u \left( x,t \right)
    - a u_x \left( x,t \right)\Delta t
    + a^2 u_{x x}\left( x,t \right) \frac{\left( \Delta t \right)^2}{2}
    + \mathcal{O}\left( \left( \Delta t \right)^3 \right)\\
.\end{equation}
The truncation errors shall be similar as we have $\Delta t \sim \Delta x$ hence $\mathcal{O} \left( \Delta t^{3} \right) \sim \mathcal{O} \left( \Delta x^{3} \right)$.
Lets start and make an super long equation by plugging \cref{eq:taylorTime,eq:taylorDeltaX,eq:taylorMDeltaX} into \cref{eq:truncationError}, to reduce some brackets $\left(\Delta \{t,x\} \right)^n$ will be expressed as $\Delta \{t,x\}^n$
\begin{align}
    L_{\Delta t} \left( x,t \right)
    &= \frac{1}{\Delta t}
    \Biggl[ 
        u \left( x,t \right)
        - a u_x \left( x,t \right)\Delta t
        + a^2 u_{x x}\left( x,t \right) \frac{ \Delta t^2}{2}
        + \mathcal{O}\left(  \Delta t^3 \right) \nonumber\\
    &- \Biggl(
            u \left( x,t \right)
            + \frac{a \Delta t}{2 \Delta x}
            \Bigl( 
                u \left( x,t \right)
                - u_x \left( x,t \right) \Delta x
                + u_{x x}\left( x,t \right) \frac{ \Delta x^2}{2}
                + \mathcal{O}\left(  \Delta x^3 \right)
                \nonumber \\
    &- \left (
                    u \left( x,t \right)
                    + u_x \left( x,t \right) \Delta x
                    + u_{x x}\left( x,t \right) \frac{ \Delta x^2}{2}
                    + \mathcal{O}\left(  \Delta x^3 \right)
                \right)
            \Biggr) \nonumber \\
    &+ \frac{a^2 \Delta t^2}{2 \Delta x ^2}
            \Biggl(
                u \left( x,t \right)
                + u_x \left( x,t \right) \Delta x
                + u_{x x}\left( x,t \right) \frac{ \Delta x^2}{2}
                + \mathcal{O}\left(  \Delta x^3 \right) \nonumber \\
    &- 2u \left( x,t \right) \nonumber \\
    &+ u \left( x,t \right)
                - u_x \left( x,t \right) \Delta x
                + u_{x x}\left( x,t \right) \frac{ \Delta x^2}{2}
                + \mathcal{O}\left(  \Delta x^3 \right)
            \Biggr)
    \Biggr]
\end{align}
When opening the brackets, doing some reformulation and regrouping the equation we derive by
\begin{equation}
    L_{\Delta t}\left( x,t \right) = \frac{1}{\Delta t}\mathcal{O}\left( \Delta t^3 \right) = \mathcal{O} \left( \Delta t^2 \right)
.\end{equation}
From this can be followed that
\begin{equation}
    \norm{L_{\Delta t}\left( \cdot,t \right)} \le C \Delta t^2
,\end{equation}
where $C = 1$.

