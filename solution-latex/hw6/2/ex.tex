\section{Exercise 2. (2D Finite Elements on Triangles) }

Given the boundary value problem

\begin{equation*}
    \begin{aligned}
        -((1+x^2)u_x)_x -((1+y^2)u_y)_y + 2u & = 2 \:\:\:\:\:\:\mathrm{in}\:T \\ u & = 0 \:\:\:\:\:\:\mathrm{on}\:\partial T.
    \end{aligned}    
\end{equation*}
% The alignment point is at '&'
\vspace{0.1cm}

On the reference triangle $T:=\{(x,y): 0< x,y < 1, x + y < 1\}$ with the vertices $(0,0), (1,0)$ and $(0,1)$

\vspace{0.2cm}

\begin{enumerate}
    \item Give the weak formulation of the problem.
    \item We want to solve the problem using piecewise finite elements. For this reason $T= T_1 \cup T_2 \cup T_3 $ is split up into three triangles $T_i$ with the common inner vertex $(x_0,y_0)=(1/3,1/3)$. Set up the linear system for the approximate solution $u_h$ but leave the matrix and vector elements in integral form.
    \item Determine the integrals of the equation system. Use a transformation $F_i$ from the triangles $T_i$ onto he reference triangle $T$. The transformation should map the common vertex $(x_0,y_0)$ onto the origin $(0,0)$.
    \item Determine the approximate solution by solving the linear system. What is the approximate value at the position $(x_0,y_0)$?
\end{enumerate}

\subsection{Solution:}

(a) For the weak formulation, one must take a test function $v \in H_{0}^1(T)$ and multiply both sides of the given PDE and integrate over the domain $T$:

$$ -\int _T\frac{\partial }{\partial x}\left(\left(1+x^2\right)\frac{\partial \:u}{\partial x}\right)\cdot v\cdot d\left(T\right)-\int _T\:\frac{\partial \:}{\partial y}\left(\left(1+y^2\right)\frac{\partial \:u}{\partial y}\right)\cdot v\cdot d\left(T\right)\:+\int _T\:2u\cdot v\cdot d\left(T\right)=\int _T\:2\cdot v\cdot d\left(T\right) $$
\vspace{0.1cm}

Using Integration by parts and considering that $v$ has compact support $\Rightarrow$

$$ - \cancelto{0}{ \left[v\cdot \left(1+x^2\right)\frac{\partial u}{\partial x}\right]_{\partial T} } +\int _T\:v'\left(1+x^2\right)\frac{\partial u}{\partial x}\:dxdy - \cancelto{0}{ \left[v\cdot \:\left(1+y^2\right)\frac{\partial u}{\partial y}\right]_{\partial T} } $$

$$ +\int _T\:v'\left(1+y^2\right)\frac{\partial u}{\partial y}\:dxdy\:+2\int _T\:uv\:dxdy=2\int _T\:v\:dxdy $$

$$ \Rightarrow  \int _T\:\left(1+x^2\right)\frac{\partial u}{\partial x}\:v'\:dxdy +  \int _T\:\left(1+y^2\right)\frac{\partial u}{\partial y}\:v'\:dxdy\:+2\int _T\:uv\:dxdy=2\int _T\:v\:dxdy $$

\vspace{0.2cm}

Therefore,
\vspace{0.3cm}

$\Rightarrow$ Find $u \in H_{0}^1(T)$ such that for all test functions $v \in H_{0}^1(T)$

\begin{equation*}
    \begin{aligned}
        a(u, v) & = F(v) \mathrm{, }\:\: \forall \:v \in H_{0}^1(T) \\ a(u,v) & = \int _T\:\left(1+x^2\right)\frac{\partial u}{\partial x}\:v'\:dxdy +  \int _T\:\left(1+y^2\right)\frac{\partial u}{\partial y}\:v'\:dxdy\:+2\int _T\:uv\:dxdy  \\ F(v) & = 2\int _T\:v\:dxdy \:
    \end{aligned}    
\end{equation*}
% The alignment point is at '&'
\vspace{0.1cm}

(b) In order to solve the problem using piecewise finite elements, one has to split up the domain $T = T_1 \cup T_2 \cup T_3 $ into three triangles $T_i$ with the common inner vertex $(x_0,y_0)=(1/3,1/3)$, as shown in the scheme bellow.

\begin{figure}[http]
    \centering
    \includegraphics[width=14cm]{2/PDE_HW6_1_1.png}
\end{figure}

For this case, using "hat" functions as basis function and considering the BC's, the basis function has 

$$ \varphi(0,0)=0 \hspace{0.9cm} \varphi(1,0)=0 \hspace{0.9cm} \varphi(0,1)=0 \hspace{0.9cm} \varphi(\frac{1}{3},\frac{1}{3})=1 $$
\vspace{0.1cm}

Considering that the basis function is one of the sort 
$$ \varphi_i(P_j) = a_i + b_i x + c_i y $$

\vspace{0.1cm}

That follows that for the triangle $T_1 = ((\frac{1}{3},\frac{1}{3}); (0,0);(1,0))$, one can find

\begin{figure}[http]
    \centering
    \includegraphics[width=14cm]{2/PDE_HW6_1_2.png}
\end{figure}

\begin{equation*}
    \begin{aligned}
        \varphi_1(P_1) & = a_1 + b_1 (1/3) + c_1 (1/3) = 1  \\ \varphi_1(P_2) & = a_1 + b_1 (0) + c_1 (0) = 0 \Rightarrow a_1 = 0 \\ \varphi_1(P_3) & = a_1 + b_1 (1) + c_1 (0) = 0 \Rightarrow b_1 = 0 \\
        & \Rightarrow c_1 = 3 \\
        \Rightarrow \varphi_1(x,y)=3y
\vspace{0.5cm}
    \end{aligned}    
\end{equation*}

\vspace{0.3cm}

For the triangle $T_2 = ((\frac{1}{3},\frac{1}{3}),(1,0);(0,1))$, one can find

\begin{figure}[http]
    \centering
    \includegraphics[width=14cm]{2/PDE_HW6_1_3.png}
\end{figure}

\begin{equation*}
    \begin{aligned}
        \varphi_2(P_1) & = a_2 + b_2 (1/3) + c_2 (1/3) = 1  \\ \varphi_2(P_2) & = a_2 + b_2 (1) + c_2 (0) = 0 \Rightarrow b_2 = -a_2 \\ \varphi_2(P_3) & = a_2 + b_2 (0) + c_2 (1) = 0 \Rightarrow c_2 = -a_2 \\
        & \Rightarrow a_2 - \frac{2a_2}{3}= 1 \Rightarrow a_2 = 3\\
        \Rightarrow \varphi_2(x,y)= 3 -3x -3y 
    \end{aligned}    
\end{equation*}

\vspace{0.4cm}

For the triangle $T_3 = ((\frac{1}{3},\frac{1}{3}),(0,1);(0,0))$, one can find

\begin{figure}[http]
    \centering
    \includegraphics[width=14cm]{2/PDE_HW6_1_4.png}
\end{figure}

\begin{equation*}
    \begin{aligned}
        \varphi_3(P_1) & = a_3 + b_3 (1/3) + c_3 (1/3) = 1  \\ \varphi_3(P_2) & = a_3 + b_3 (0) + c_3 (1) = 0 \Rightarrow c_3 = -a_3 \\ \varphi_3(P_3) & = a_3 + b_3 (0) + c_3 (0) = 0 \Rightarrow a_3 = 0 \\
        & \Rightarrow c_3 = 0 \Rightarrow b_3 = 3\\
        \Rightarrow \varphi_1(x,y)=3x
\vspace{0.5cm}
    \end{aligned}    
\end{equation*}

\vspace{0.5cm}

Therefore, for the approximate solution $u_h$, the system $ \alpha a = b$ has the form

\begin{equation*}
    \begin{aligned}
        a & = \int _T\:\left(1+x^2\right) \frac{\partial \varphi}{\partial x}\:\frac{\partial \varphi}{\partial x} \:dxdy +  \int _T\:\left(1+y^2\right)\frac{\partial \varphi}{\partial y}\:\frac{\partial \varphi}{\partial y}\:dxdy\:+2\int _T\:\varphi\:\varphi\:dxdy  \\ b & = 2\int _T\varphi\:dxdy \:
    \end{aligned}    
\end{equation*}
% The alignment point is at '&'
\vspace{0.1cm}

\newpage

(c) From the lecture notes, one knows that

$$ \Tilde{A}_i \begin{pmatrix}\xi _1\\ \:\xi _2\end{pmatrix} = P_1 + (P_2 - P_1, P_3 - P_1) \begin{pmatrix}\xi _1\\ \:\xi _2\end{pmatrix} $$

Therefore, 
\vspace{0.3cm}

For $ \Tilde{A}_1 : \hat{T} \rightarrow T_1, $

$$ \Tilde{A}_1 \begin{pmatrix}\xi _1\\ \xi _2\end{pmatrix} = \begin{pmatrix}\frac{1}{3}\\ \frac{1}{3}\end{pmatrix}+\begin{pmatrix}0-\frac{1}{3}&1-\frac{1}{3}\\ 0-\frac{1}{3}&0-\frac{1}{3}\end{pmatrix}\begin{pmatrix}\xi _1\\ \xi _2\end{pmatrix}=\begin{pmatrix}\frac{1}{3}\\ \frac{1}{3}\end{pmatrix} + \underbrace{ \begin{pmatrix}-\frac{1}{3}&\frac{2}{3}\\ -\frac{1}{3}&-\frac{1}{3}\end{pmatrix}}_{A_1} \begin{pmatrix}\xi _1\\ \xi _2\end{pmatrix}  $$
\vspace{0.2cm}


The same goes for 
\vspace{0.3cm}

$ \Tilde{A}_2 : \hat{T} \rightarrow T_2, $

$$ \Tilde{A}_2 \begin{pmatrix}\xi _1\\ \xi _2\end{pmatrix} =\begin{pmatrix}\frac{1}{3}\\ \frac{1}{3}\end{pmatrix}+ \underbrace{\begin{pmatrix}\:\:\:\frac{2}{3}&-\frac{1}{3}\\ -\frac{1}{3}&\:\:\:\frac{2}{3}\end{pmatrix}}_{A_2}\begin{pmatrix}\xi _1\\ \xi _2\end{pmatrix} $$

$ \Tilde{A}_3 : \hat{T} \rightarrow T_3, $

$$ \Tilde{A}_3 \begin{pmatrix}\xi _1\\ \xi _2\end{pmatrix} =\begin{pmatrix}\frac{1}{3}\\ \frac{1}{3}\end{pmatrix}+\underbrace{\begin{pmatrix}-\frac{1}{3}&-\frac{1}{3}\\ \:\:\:\frac{2}{3}&-\frac{1}{3}\end{pmatrix}}_{A_3}\:\begin{pmatrix}\xi _1\\ \xi _2\end{pmatrix} $$

\vspace{0.2cm}

Now, for obtaining the transformations $F_i = \Tilde{A}^{-1}_{i}$ (from the lecture notes)

$$ \int _{\Tilde{A}_i (\hat{T})} f\left(x,y\right)d\left(x,y\right)=\int _{\hat{T}}\:f\left(\Tilde{A}_i\left(\xi _1,\xi _2\right)\right)\left|det(A_i\left(\xi _1,\xi _2\right))\right|d\left(\xi _1,\xi _2\right)\:\: $$
\vspace{0.2cm}

Considering a $\hat{\varphi}$ corresponding to the basis function $\varphi$ on the reference element $\hat{T}$, then the following applies to the derivatives

$$ \nabla _{\left(\xi _1,\xi _2\right)}\:\hat{\varphi }\left(\xi _1,\xi _2\right)=\nabla _{\left(\xi _1,\xi _2\right)}\:\varphi \left(x\left(\xi _1,\xi _2\right),y\left(\xi _1,\xi _2\right)\right)=\nabla _{\left(\xi _1,\xi _2\right)}\:\varphi \left(x,y\right)\cdot A_i $$
\vspace{0.2cm}

Now, by breaking down the integral of $a$ (from item (b)) into three integrals in the domains $T_1, T_2$ and $T_3$, one can describe $a$ as the sum of 

$$ \int _{T_{i}}\:\left(1+x^2\right) \frac{\partial \varphi}{\partial x}\:\frac{\partial \varphi}{\partial x} \:dxdy +  \int _{T_{i}}\:\left(1+y^2\right)\frac{\partial \varphi}{\partial y}\:\frac{\partial \varphi}{\partial y}\:dxdy\:+2\int _{T_{i}}\:\varphi\:\varphi\:dxdy = $$

$$ \int _{T_i}\:\nabla \varphi ^T\begin{pmatrix}1+x^2&0\\ 0&1+y^2\end{pmatrix}\nabla \varphi \:dxdy\:+\:2\int _{T_i}\:\varphi \:\varphi \:dxdy\: $$


$$ =\int _{\hat{T}}\left[(A_i^{-1} \nabla \hat{\varphi} )^T\begin{pmatrix}1+\left(\Tilde{A_i}\left(\xi _1,\xi _2\right)_1\right)^2&0\\ \:0&1+\left(\Tilde{A_i}\left(\xi _1,\xi _2\right)_2\right)^2\end{pmatrix}A_i^{-1}\:\nabla \hat{\varphi }\:+2\hat{\varphi }\hat{\varphi }\right]\left|det\:A_i\right|d\left(\xi _1,\xi _2\right) $$
\vspace{0.2cm}

By calculating the determinants of the matrices $A_i$, one obtains 

$$ |det A_i| = |det A_1| = |det A_2| = |det A_3| = \frac{1}{3} $$
\vspace{0.2cm}

Additionally, one can compute the inverse matrices of $A_i$, 

$$ manually \Rightarrow \begin{pmatrix}a\:&\:b\:\\ c\:&\:d\:\end{pmatrix}^{-1}=\frac{1}{\det \begin{pmatrix}a\:&\:b\:\\ c\:&\:d\:\end{pmatrix}}\begin{pmatrix}d\:&\:-b\:\\ -c\:&\:a\:\end{pmatrix} $$
\vspace{0.2cm}
$$ A^{-1}_{1} = \begin{pmatrix}-1&-2\\ 1&-1\end{pmatrix} \hspace{0.5cm } A^{-1}_{2} = \begin{pmatrix}2&1\\ 1&2\end{pmatrix} \hspace{0.5cm } A^{-1}_{3} = \begin{pmatrix}-1&1\\ -2&-1\end{pmatrix} $$

\vspace{0.6cm}

Furthermore, one can refer for the point $(\xi_1,\xi_2)=(0,0)$ and its gradient 

$$ \hat{\varphi}(\xi_1,\xi_2)=1 - \xi_1 - \xi_2 $$

$$ \nabla \hat{\varphi }\left(\xi _1\xi _2\right)=\begin{pmatrix}-1\\ -1\end{pmatrix} $$

\vspace{0.3cm}
Now, considering the Gaussian quadrature of degree N for triangles:

$$ \int \:\int _{T_{st}}g\left(\xi _1\xi _2\right)d\xi _1d\xi _2\:=\frac{1}{2}\sum ^{N_g}_{i=1}g\left(\xi _{1,i}\xi _{2,i}\right), \:\forall g\left(\xi _1\xi _2\right)\:\in P_N\left(\xi _1\xi _2\right) $$

where $N_g$ is the number of quadrature points, ($\xi_{1,i}, \xi_{2,i}$) are quadrature points located inside the standard triangle and $w_i$ are weights (normalized with respect to the triangle area).
\vspace{0.2cm}

For the case of this problem $N=2 \Rightarrow P_2\left(\xi _1\xi _2\right) = span\{ 1,\xi_1,\xi_2,\xi_{1}^{2},\xi_{2}^{2}, \xi_1 \xi_2 \}$
\vspace{0.4cm}

Gaussian quadrature of degree 2 for a standard triangle $T_{st}$

$$ \int \:\int _{T_{st}}g\left(\xi _1\xi _2\right)d\xi _1d\xi _2\:=\frac{1}{2}\left[\frac{1}{3}g\left(\frac{1}{6},\frac{1}{6}\right)+\frac{1}{3}g\left(\frac{2}{3},\frac{1}{6}\right)+\frac{1}{3}g\left(\frac{1}{6},\frac{2}{3}\right)\right] $$
\vspace{0.2cm}

Now, given the proper tools, one can solve the integrals for $a$

$$ \int _{T_i}\varphi \varphi \:d\left(x,y\right)\:=\frac{1}{3}\int _{\hat{T}}\:\hat{\varphi }^2d\left(\xi _1,\xi _2\right)=\frac{1}{3}\int _{\hat{T}}\left(1-\xi _1-\xi _2\right)^2d\left(\xi _1,\xi _2\right) = $$
\vspace{0.2cm}

Using Gaussian quadrature of degree 2 $\Rightarrow$

$$ =\frac{1}{3}\cdot \frac{1}{2}\frac{1}{3}\left[\left(1-\frac{1}{6}-\frac{1}{6}\right)^2+\left(1-\frac{2}{3}-\frac{1}{6}\right)^2+\left(1-\frac{1}{6}-\frac{2}{3}\right)^2\right]= \frac{1}{36} $$
\vspace{0.2cm}

Therefore, for $T=\sum^{3}_{i=1} T_i$
\vspace{0.2cm}

$$ \int _T\:2\:\varphi ^2\:dxdy\:=\:2\left(\frac{1}{36}+\frac{1}{36}+\frac{1}{36}\right)=\frac{1}{6} $$

\vspace{0.4cm}

Considering the integrals for each $T_i$, one can compute

$$ \int _{\hat{T}}\left[(A_1^{-1} \nabla \hat{\varphi} )^T\begin{pmatrix}1+\left(\Tilde{A_1}\left(\xi_1,\xi_2\right)_1\right)^2&0\\ \:0&1+\left(\Tilde{A_1}\left(\xi_1,\xi_2\right)_2\right)^2\end{pmatrix}A_1^{-1}\:\nabla \hat{\varphi }\:+2\hat{\varphi }\hat{\varphi }\right]\left|det\:A_1\right|d\left(\xi_1,\xi_2\right) = $$

$$ = \frac{1}{3}\int _{\hat{T}}\:9+9\left(\frac{1}{3}-\frac{1}{3}\xi _1-\frac{1}{3}\xi _2\right)^2\:d\left(\xi _1,\xi _2\right)\:= $$
\vspace{0.2cm}

By also using Gaussian quadrature of degree 2 $\Rightarrow$

$$ = \frac{1}{3}\cdot \:\frac{1}{2}\frac{1}{3}\left[\left(\frac{1}{3}-\frac{1}{3}\frac{1}{6}-\frac{1}{3}\frac{1}{6}\right)^2+\left(\frac{1}{3}-\frac{1}{3}\frac{2}{3}-\frac{1}{3}\frac{1}{6}\right)^2+\left(\frac{1}{3}-\frac{1}{3}\frac{1}{6}-\frac{1}{3}\frac{2}{3}\right)^2\right]= $$ 
$$ = \frac{1}{18}\left[\frac{85}{9}+\frac{325}{36}+\frac{325}{36}\right]=\frac{55}{36} $$
\vspace{0.2cm}

The same procedure follows for $T_1$ and $T_2$ 
\vspace{0.2cm}

$$ \int _{\hat{T}}\left[(A_2^{-1} \nabla \hat{\varphi} )^T\begin{pmatrix}1+\left(\Tilde{A_2}\left(\xi_1,\xi_2\right)_1\right)^2&0\\ \:0&1+\left(\Tilde{A_2}\left(\xi_1,\xi_2\right)_2\right)^2\end{pmatrix}A_2^{-1}\:\nabla \hat{\varphi }\:+2\hat{\varphi }\hat{\varphi }\right]\left|det\:A_2\right|d\left(\xi_1,\xi_2\right) = $$

$$ = \frac{1}{3}\int _{\hat{T}}\:18+9\left(\frac{1}{3}+\frac{2}{3}\xi _1-\frac{1}{3}\xi _2\right)^2\: + 9\left(\frac{1}{3}-\frac{1}{3}\xi _1+\frac{2}{3}\xi _2\right)^2\:d\left(\xi _1,\xi _2\right)\:= $$
\vspace{0.2cm}

By also using Gaussian quadrature of degree 2 $\Rightarrow$
\vspace{0.2cm}

$$ = \frac{1}{3}\cdot \:\frac{1}{2}\frac{1}{3} \left[\frac{373}{18}+\frac{833}{36}+\frac{833}{36}\right]=\frac{67}{18} $$
\vspace{0.2cm}

$$ \int _{\hat{T}}\left[(A_3^{-1} \nabla \hat{\varphi} )^T\begin{pmatrix}1+\left(\Tilde{A_3}\left(\xi_1,\xi_2\right)_1\right)^2&0\\ \:0&1+\left(\Tilde{A_3}\left(\xi_1,\xi_2\right)_2\right)^2\end{pmatrix}A_3^{-1}\:\nabla \hat{\varphi }\:+2\hat{\varphi }\hat{\varphi }\right]\left|det\:A_3\right|d\left(\xi_1,\xi_2\right) = $$

$$ = \frac{1}{3}\int _{\hat{T}}\:9+9\left(\frac{1}{3}-\frac{1}{3}\xi _1-\frac{1}{3}\xi _2\right)^2\:d\left(\xi _1,\xi _2\right)\:= $$

\vspace{0.3cm}

Gaussian quadrature of degree 2 $\Rightarrow$
\vspace{0.2cm}

$$ = \frac{1}{3}\cdot \:\frac{1}{2}\frac{1}{3} \left[\frac{85}{9}+\frac{325}{36}+\frac{325}{36}\right]=\frac{55}{36} $$
\vspace{0.3cm}

Additionally, for the R.H.S (b value) $\Rightarrow$

$$ b = 2\int _T\:\varphi \:dxdy\:=3 \cdot 2\cdot \:\frac{1}{3}\:\int _{\hat{T}}\:\hat{\varphi }\:d\left(\xi _1,\xi _2\right)\:=\:3 \cdot \frac{2}{3}\:\int _{\hat{T}}\left(1-\xi _1-\xi _2\right)d\xi _1d\xi _2$$
\vspace{0.2cm}

By using a Gaussian quadrature of degree 1 for a standard triangle $Tst$

$$ \int \int _{T_{st}}g\left(\xi _1\xi _2\right)d\xi _1d\xi _2\:=\frac{1}{2}g\left(\frac{1}{3},\frac{1}{3}\right), \:\:\forall g\left(\xi _1\xi _2\right)\:\in P_1\left(\xi _1\xi _2\right) $$

$$ b= 3 \cdot \frac{2}{3}\:\int _{\hat{T}}\left(1-\xi \:_1-\xi \:_2\right)d\xi _1d\xi \:_2\:= 3\cdot \frac{2}{3}\frac{1}{2}\left(1-\frac{1}{3}-\frac{1}{3}\right)=3\cdot \frac{2}{3}\frac{1}{2}\frac{1}{3}=\frac{6}{18}=\frac{1}{3} $$
\vspace{0.2cm}

(d) For the system $a \alpha = b$ and considering the value $(x_0,y_0)=(\frac{1}{3},\frac{1}{3})$

$$ \left(\frac{1}{6}+\frac{55}{36}+\frac{67}{18}+\frac{55}{36}\right)\alpha =\frac{1}{3}\: \Rightarrow \frac{125}{18}\alpha=\frac{1}{3} $$

$$\Rightarrow  \alpha = \frac{6}{125}$$
