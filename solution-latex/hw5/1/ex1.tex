\section{Exercise 1}
\subsection{Task}
Consider the following one dimensional boundary value problem:
\begin{subequations}
    \label{eq:strongForm}
    \begin{align}
        -u'' + u &= f(x) && \text{in } (0,1) \subset \mathbb{R}, f \in L^{2}((0,1)),\\
        u(0) &= 0 && \text{on } \Gamma_{D},\\
        \alpha u(1) + u'(1) &= a, && \alpha \ge 0, a \in \mathbb{R} \text{ on } \Gamma_{R}.
    \end{align}
\end{subequations}

\begin{enumerate}[label={\alph*)}]
    \item Derive the weak formulation of the above problem.
        \item Find the discrete solution using Finite Element Method.
            Choose the continuous piecewise linear functions
            \begin{equation}
                \label{eq:basisFuncitons}
                \psi_{i}(x) := 
                \begin{cases}
                    \frac{x - x_{i-1}}{h_i}, & x \in I_i = (x_{i-1}, x_i)\\
                    \frac{x_{i+1}-x}{h_{i+1}}, & x \in I_{i+1} = (x_i, x_{i+1})\\
                    0, & \text{otherwise}
                \end{cases}
            \end{equation}
            as basis functions on a uniform grid, $0 = x_0 < x_1 < ... < x_N = 1$ with $h= x_i - x_{i-1}$.
\end{enumerate}

\subsection{Solution - Weak Formulation}
To derive the weak formulation \cref{eq:strongForm} is multiplied by the ansatz function $v \in \mathcal{H}_{0}^{1}\left(0,1\right)$ delivers
\begin{equation}
    -v u'' + v u = v f(x).
\end{equation}
Partial integration with respect to the boundary conditions allows to rewrite the equation as:
\begin{align}
    \int_{\Omega}^{} - v \Delta u \: d{x} 
    + \int_{\Omega}^{} v u \: d{x}
    &= \int_{\Omega}^{} v f(x) \: d{x}\nonumber\\
    \int_{\Omega}^{} \nabla v \nabla u \: d{x}
    - \int_{\partial \Omega}^{} v \textbf{n} u \: d{x}
    + \int_{\Omega}^{} v u \: d{x}
    &= \int_{\Omega}^{} v f(x) \: d{x}\nonumber\\
    \int_{\Omega}^{} \nabla v \nabla u \: d{x}
    - \int_{\partial \Omega}^{} v (a - \alpha u(1)) \: d{x}
    + \int_{\Omega}^{} v u \: d{x}
    &= \int_{\Omega}^{} v f(x) \: d{x}\nonumber\\
    \int_{\Omega}^{} \nabla v \nabla u \: d{x}
    + \int_{\partial \Omega}^{} v \alpha u(1) \: d{x}
    + \int_{\Omega}^{} v u \: d{x}
    &= \int_{\Omega}^{} v f(x) \: d{x}
    + \int_{\partial \Omega}^{} v a \: d{x}\nonumber\\
    \text{and evaluation of the boundary integrals}\nonumber\\
    \int_{\Omega}^{} \nabla v \nabla u \: d{x}
    + \int_{\Omega}^{} v u \: d{x}
    + v \alpha u(1)
    &= \int_{\Omega}^{} v f(x) \: d{x}
    +  v a \nonumber\\
    \text{with respect to the specific domain}\nonumber\\
    \label{eq:weakForm}
    \int_{0}^{1} \nabla v \nabla u \: d{x}
    + \int_{0}^{1} v u \: d{x}
    + v(1) \alpha u(1)
    &= \int_{0}^{1} v f(x) \: d{x}
    +  v(1) a    
\end{align}
Using \cref{eq:weakForm} we can state the weak formulation as:
\begin{align}
    \label{eq:weakFormStated}
    \text{Find } 
    u \in \mathcal{H}_{0}^{1}\left(0,1\right)
    \text{ such that for all }
    v &\in \mathcal{H}_{0}^{1}\left(0,1\right):\\
    a(u,v) &= F(v), \nonumber\\
    \text{where }
    a(u,v) &= 
    \int_{0}^{1} \nabla v \nabla u \: d{x}
    + \int_{0}^{1} v u \: d{x}
    + v(1) \alpha u(1),\nonumber\\
    \text{and } 
    F(v) &=
    \int_{0}^{1} v f(x) \: d{x}
    +  v(1) a\nonumber.
\end{align}

\subsection{Solution - Discrete Formulation}

With the basis functions from \cref{eq:basisFuncitons}, the approximation of $u$ in \cref{eq:weakFormStated} can be formulated as:
\begin{equation}
    u_{N} = \sum_{j=1}^{N} \alpha_{j} \psi_{j}\left(x\right) \text{ with } \alpha_j \in \mathbb{R}.
\end{equation}
where $u_{N}$ belongs to the space $\mathcal{V}_{N}$ of continuous piecewise linear functions given with:
\begin{equation}
    \mathcal{V}_{N} = \{u | u \in \mathcal{H}_{0}^{1}\left(0,1\right), u(0) = 0 \}.
\end{equation}
The basis functions do satisfy the Dirichlet boundary condition at $x=0$.
Plugging this discretization into \cref{eq:weakFormStated} allows write the discrete equation as:
\begin{align}
    \label{eq:weakFormStatedDiscrete}
    \text{Find } 
    u_{N} \in \mathcal{V}_{N}
    \text{ such that for all }
    v_{N} &\in \mathcal{V}_{N}:\nonumber\\
    a(u_{N},v_{N}) &= F(v_{N}).
\end{align}
Writing $a, F$ in terms of $\psi$ and introducing $A = a(u,v)$
\begin{subequations}
    \label{eq:weakDiscreteFormulation}
    \begin{align}
        A_{ij} &= a \left(\psi_i, \psi_j\right)\nonumber\nonumber\\
               &= \int_{0}^{1} \psi_i' \psi_j'\: d{x}
               + \int_{0}^{1} \psi_i \psi_j \: d{x}
               + \psi_i (1) \alpha \psi_j (1) \\
        F_j &= \int_{0}^{1} \psi_j f(x) \: d{x}
        + \psi_j (1) a
    \end{align}
\end{subequations}
The integrals that do appear in \cref{eq:weakDiscreteFormulation} can already be evaluated considering the three possible cases for  $j$ and $j$ as:
\begin{equation}
    \int_{0}^{1} \psi_i'  \psi_j' \: d{x} =
    \begin{cases}
        \frac{2}{h}, & i=j,\\
        -\frac{1}{h}, & j=i \pm 1,\\
        0, & \text{otherwise}.
    \end{cases}
\end{equation}
as well as
\begin{equation}
    \int_{0}^{1} \psi_i  \psi_j \: d{x} =
    \begin{cases}
        \frac{2h}{3}, & i=j,\\
        \frac{h}{6}, & j=i \pm 1,\\
        0, & \text{otherwise}.
    \end{cases}
\end{equation}
which can be combined and written for the inner nodes of $A_{ij}$ as:
\begin{equation}
    \label{eq:aInnerNodes}
    a(\psi_i, \psi_j) = 
    \begin{cases}
        \frac{2}{h} + \frac{2h}{3}, &i=j\\
        - \frac{1}{h} + \frac{h}{6}, & j = i \pm 1\\
        0, & \text{otherwise}
    \end{cases}
\end{equation}
and for the nodes $a(\psi_{N-1}, \psi_{N})$,$a(\psi_{N}, \psi_{N-1})$ the Robin boundary evaluates to,
\begin{equation}
    \label{eq:aOuterNodes}
    a(\psi_{N-1}, \psi_{N}) = a(\psi_{N}, \psi_{N-1}) = -\frac{1}{h} + \frac{h}{6}
\end{equation}
as in the inner nodes since the product $\psi_{N-1}(1) a \psi_{N}(1)$ evaluates to zero.
For the end node $a(\psi_N (1), \psi_N(1))$ is expressed as
\begin{equation}
    \label{eq:aOuterRobin}
    a(\psi_N, \psi_N) = \frac{2}{h} + \frac{2h}{3} + \alpha
\end{equation}
Meaning that the whole matrix $A$ can be seen as sparse.
The right hand side $F_{j}$ is given over
\begin{equation}
    \label{eq:rhs}
    F_{j} =
    \begin{cases}
        \int_{x_{j-1}}^{x_{j+1}} f(x) \psi_j(x) \: d{x}, & j =1,..., N-1\\
        \int_{x_{j-1}}^{x_{j}} f(x) \psi_j(x) \: d{x} + a, & j =N
    \end{cases}
\end{equation}
The solution of \cref{eq:strongForm} can then be obtained by solving the system of linear equations over \cref{eq:aInnerNodes,eq:aOuterNodes,eq:aOuterRobin,eq:rhs} as:
\begin{equation}
    \sum_{i=1}^{N} a(\psi_i, \psi_j)u_{N} = F_{j}, \qquad \forall j = 1,...,N
\end{equation}






% todo add the formulation and finish it
