\section{Exercise 2}

We again consider the one-dimensional Poisson problem

\begin{equation*}
    \begin{aligned}
        - u'' = f \:\:\mathrm{in}\:(0,1), \:\:u(0) = 0 = u(1).
    \end{aligned}    
\end{equation*}
% The alignment point is at '&'
\vspace{0.1cm}

The domain $\Omega : = (0,1)$ is equipped with the grid $0 = x_0 < x_1 < ... < x_N = 1$.
\vspace{0.2cm}

In contrast to the previous task, we now consider the approximation space $V_h$ of continuous, piecewise quadratic functions, i.e.,

$$ V_h=\left\{u\:\in C^0\left(\left[0,1\right]\right):\:u_{\left[x_{k-1},\:x_k\right]}=a_k\:+b_kx\:+c_kx^2,\:k=1,\:...,\:N,\:\:u\left(0\right)=0=u\left(1\right)\right\} $$

\vspace{0.2cm}
We equip this space with basis functions of the form displayed in Figure 1. To be precise, the basis consists of the usual 
$N$-1 hat functions $\left\{\phi _{2k}\right\}_{k=1}^{N-1}$ together with an additional $N$ functions $\left\{\phi _{2k-1}\right\}_{k=1}^{N}$ defined as

$$ \phi_{2k-1}\left(x\right)\:=\:\begin{cases}a+bx+cx^2&\mathrm{for}\:\:x\:\in \left(x_{k-1},\:x_k\right)\:\:\mathrm{with}\:\:\phi_{2k-1}\left(\frac{x_{k-1}+x_k}{2}\right)=1\:\\ \:0&\mathrm{otherwise}\end{cases} $$

\vspace{0.2cm}
Write down the Galerkin discretisation of this problem for the approximation space $V_h$. Compute the stiffness matrix $A$ using the definition of the basis $\left\{\phi_{i}\right\}_{i=1}^{2N-1}$ of hat-functions. Show your computations.

\subsection{Solution - Galerkin Discretization}
In order to derive the Ritz-Galerkin (RG) equations and find the stiffness matrix, one must first describe the Weak formulation of the problem.

By taking a test function $v \in H_{0}^1(\Omega)$ and multiplying the given D.E. and integrating over the domain $\Omega=(0,1)$:

$$ -\int _{\Omega }v\cdot u''\:d\Omega\:=\:\int _{\Omega \:}v\cdot f\:d\Omega\: $$

\vspace{0.3cm}
When integrating by parts

$$ \Rightarrow  -\left(  \cancelto{0}{\left[v\:u'\right]_0^1}\:-\:\int _{_{\Omega }}v'\:u'\:d\Omega \right)=\int _{_{\Omega \:}}v\:f\:d\Omega \: $$

The first parcel on the L.H.S goes to zero since $v$ vanishes at the boundary.

$$ \:\int _{_{\Omega \:}}v'\:u'\:d\Omega \:=\int _{_{\Omega \:}}v\:f\:d\Omega \: $$

\vspace{0.3cm}
Therefore the Galerkin Discretization is given by:
\vspace{0.4cm}

Find $u_h \in V_h$ such that for all test functions $v_h \in V_h$

\begin{equation*}
    \begin{aligned}
        a(u_h,v_h) & = F(v_h) \mathrm{, }\:\: \forall \:v_h \in V_h \\ a(u_h,v_h) & = \:\int _0^1\:v'_h\left(x\right)\:u'_h\left(x\right)\:dx\: \\ F(v_h) & = \int _0^1\:v_h\left(x\right)\:f\left(x\right)\:dx\:
    \end{aligned}    
\end{equation*}
% The alignment point is at '&'
\vspace{0.1cm}

Now, by denoting the $\left\{\phi_{i}\right\}_{i=1}^{2N-1}$ as the basis functions of the $V_h$ space, the Galerkin discretization results in a stiffness matrix $A$ with entries in the form  

$$ A_{i,j} = \sum _{k=1}^N\:A^{\left(k\right)}_{i,j} $$

and the element matrices are given by

$$ A^{\left(k\right)}_{i,j}=\int _{I_k}\phi '_i\:\phi '_j\:dx\:  $$


Where $I_k$ is the interval $\left(x_{k-1},\:x_k\right)$

\vspace{0.5cm}
For the evaluation of the integrals, one must map the sub-interval $I_k$ to the reference interval $(a,b)=(0,1)$.
By defining $a:= x_{k-1}, b:= x_{k}$ and $h_k:= b-a$, one can consider a variable substitution ($x \rightarrow  y$) for the sub-interval and define the following functions based on the given figure

\begin{equation*}
    \begin{aligned}
        \hat{\phi_{1}}(y) & := 1-y, \:\: \forall \:y \in (0,1) \\  \hat{\phi_{2}}(y) & := 4y -4y^2, (a_k=0, b_k=4, c_k=-4) \:\: \forall \:y \in (0,1) \\  \hat{\phi_{3}}(y) & := y, \:\: \forall \:y \in (0,1)
    \end{aligned}    
\end{equation*}
% The alignment point is at '&'
\vspace{0.1cm}

Therefore, one can map

$$ \frac{b-a}{1-0}=\frac{\Delta x}{\Delta y} \Rightarrow \Delta x\:=\:h\:\cdot \Delta y \Rightarrow x-x_{k-1}\:=\:h\:\cdot \left(y-0\right)\:$$

$$ \Rightarrow x\:-a=\:h\:\cdot y \Rightarrow x\:=\:a+h\:\cdot y \Rightarrow \frac{dx}{dy}\:=\:h $$

Considering that 

$$ \phi _k\left(x\right)=\phi _k\left(a+h\cdot y\right)=\hat{\phi_{k}} \left(y\right) $$

$$ \frac{d\:\phi _k\left(x\right)}{dx}=\frac{d\:\hat{\phi _k}\left(y\right)}{dy}\cdot \frac{dy}{dx}=\hat{\phi_k}'\left(y\right)\cdot \frac{1}{h} $$
\vspace{0.2cm}

One can show the following integrals for the different cases:
\vspace{0.2cm}

$$ (i=j=2k-2) \Rightarrow \int _{I_k}\phi '_{2k-2}\cdot \:\phi \:'_{2k-2}\:dx=\int _0^1\frac{1}{h_k}\:\hat{\phi} \:'_1\cdot \frac{1}{h_k}\:\hat{\phi} \:\:'_1\cdot h_k\:dy = \frac{1}{h_k}$$

$$ (i=j=2k) \Rightarrow \int _{I_k}\phi '_{2k}\cdot \:\phi \:'_{2k}\:dx=\int _0^1\frac{1}{h_k}\:\hat{\phi} \:'_1\cdot \frac{1}{h_k}\:\hat{\phi} \:\:'_1\cdot h_k\:dy = \frac{1}{h_k}$$

$$ (i=j=2k-1) \Rightarrow \int _{I_k}\phi '_{2k-1}\cdot \:\phi \:'_{2k-1}\:dx=\int _0^1\frac{1}{h_k}\:\hat{\phi} \:'_2\cdot \frac{1}{h_k}\:\hat{\phi} \:\:'_2\cdot h_k\:dy = \frac{1}{h_k}\int _0^1\left(4-8y\right)^2dy= \frac{16}{3} \cdot \frac{1}{h_k}$$

$$ (|i-j| = 2) \Rightarrow \int _{I_k}\phi '_{2k-2}\cdot \:\phi \:'_{2k}\:dx=\int _0^1\frac{1}{h_k}\:\hat{\phi} \:'_1\cdot \frac{1}{h_k}\:\hat{\phi} \:\:'_3\cdot h_k\:dy = - \frac{1}{h_k}$$

$$ (|i-j|=1) \Rightarrow \int _{I_k}\phi '_{2k-2}\cdot \:\phi \:'_{2k-1}\:dx=\int _0^1\frac{1}{h_k}\:\hat{\phi} \:'_1\cdot \frac{1}{h_k}\:\hat{\phi} \:\:'_2\cdot h_k\:dy = \frac{1}{h_k}\int _0^1\left(-1\right)\left(4-8y\right)dy= $$
$$ = \frac{1}{h_k}\left[-4y+4y^2\right]_{0}^{1} = \frac{1}{h_k}\cdot \:0 = 0 $$

$$ (|i-j|=1) \Rightarrow \int _{I_k}\phi '_{2k}\cdot \:\phi \:'_{2k-1}\:dx=\int _0^1\frac{1}{h_k}\:\hat{\phi} \:'_3\cdot \frac{1}{h_k}\:\hat{\phi} \:\:'_2\cdot h_k\:dy = \frac{1}{h_k}\int _0^1\left(+1\right)\left(4-8y\right)dy= $$
$$ = \frac{1}{h_k}\left[4y-4y^2\right]_{0}^{1} = \frac{1}{h_k}\cdot \:0 = 0 $$

\vspace{0.4cm}

Thus the stiffness matrix $A$ has the form as shown 

$$ A_{i,\:j}=\begin{pmatrix}a\left(\phi _1,\:\phi _1\right)&a\left(\phi _1,\:\phi _2\right)&a\left(\phi _1,\:\phi _3\right)&0&&\\ a\left(\phi _2,\:\phi _1\right)&a\left(\phi _2,\:\phi _2\right)&0&a\left(\phi _2,\:\phi _4\right)&&\\ a\left(\phi _3,\:\phi _1\right)\:&0&a\left(\phi _3,\:\phi _3\right)&0&a\left(\phi _3,\:\phi _5\right)&\\ \:0&a\left(\phi _4,\:\phi _2\right)&0&...&0&...\\ \:&&a\left(\phi _5,\:\phi _3\right)&0&...&0\\ \:&&&...&0&a\left(\phi _i,\:\phi _i\right)\end{pmatrix} $$

One can notice (by some examples) that 

for $a\left(\phi _{1},\phi \:_{1}\right) \Rightarrow k=1 \Rightarrow$ case $(2k-1) \Rightarrow a_{11} = \frac{16}{3} \cdot \frac{1}{h_1}$ 

for $a\left(\phi _{2},\phi _{2}\right) \Rightarrow k=2, k=1 \Rightarrow$ cases $(2k-2$ and $2k) \Rightarrow a_{22} = \frac{1}{h_2} + \frac{1}{h_1}$

for $a\left(\phi _{3},\phi _{3}\right) \Rightarrow k=2 \Rightarrow $ case $ (2k-1) \Rightarrow a_{33} = \frac{16}{3} \cdot \frac{1}{h_2}$

for $a\left(\phi _{2},\phi _{1}\right) \Rightarrow k=1 \Rightarrow $ case $ (2k, 2k-1) \Rightarrow a_{21} = 0 $

for $a\left(\phi _{2},\phi _{4}\right) \Rightarrow k=2 \Rightarrow $ case $ (2k-2, 2k) \Rightarrow a_{24} = - \frac{1}{h_2}  $

Therefore

$$ A_{i,\:j}= \begin{pmatrix}\frac{1}{h_1}\frac{16}{3}&0&...&&&\\ 0&\frac{1}{h_2}+\frac{1}{h_1}&0&-\frac{1}{h_2}&&\\ ...&0&\frac{1}{h_2}\frac{16}{3}&0&...&\\ &-\frac{1}{h_2}&0&\frac{1}{h_3}+\frac{1}{h_2}&...&\\ &&...&...&...&\\ &&&&&...\end{pmatrix} $$

By assuming $h=h_k \Rightarrow$

$$ A =\frac{1}{h}\begin{pmatrix}\frac{16}{3}&0&...&&&\\ 0&2&0&-1&&\\ ...&0&\frac{16}{3}&0&...&\\ &-1&0&2&...&\\ &&...&...&...&\\ &&&&&...\end{pmatrix} $$
