\section[Exercise 3]{Exercise 3 - Burgers' equation }

Consider the Burgers' equation

$$ u_t + u u_x = 0, \:\:\mathrm{for}\:\: (x,t) \in \mathbb{R} \times (0,T] $$

with the following initial conditions

$$ u\left(x,t\right)=u_0\left(x\right)=\begin{cases}1,\:\:\:\:\:\:&x<0,\\ 1-x,&0\le x\le 1,\:x\:\in \mathbb{R},\\ 0,\:\:\:\:\:\:&x\ge 1.\end{cases} $$

\vspace{0.3cm}

\begin{enumerate}[label=(\alph*),leftmargin=1.35cm]
    \item Determine the characteristics and sketch them in the $x-t$ plane.
    \item Let $t_s$ be the time at which the characteristics \textit{first} intersect each other and form a shock. Compute this time $t_s$.
    \item Find the solution $u(x,t)$ for time $t < t_s$.
\end{enumerate}

\vspace{0.2cm}

\subsection{Solution a)}

\textbf{(a)} By applying the Method of the Characteristics one has to verify that $u(x,t)$ does indeed remain constant along the characteristics. One can differentiate $u$ along one of these curves to find the rate of change of $u$ along the characteristic:

\vspace{0.2cm}

$$ \frac{du\left(x\left(t\right),t\right)}{dt}=\frac{\partial u\left(x\left(t\right),t\right)}{\partial x}\cdot \frac{dt}{dt}+\frac{\partial u\left(x\left(t\right),t\right)}{\partial t}=0 $$


$$ \Rightarrow u_t = - u_x \cdot \frac{dx}{dt} $$

\vspace{0.2cm}

By comparing with the Burger's equation

$$ u_t + u u_x = 0 $$

$$ \Rightarrow - u_x \cdot \frac{dx}{dt} + u\cdot  u_x = 0 \Rightarrow  u = \frac{dx}{dt} $$

$$ \Rightarrow u(x,t) = \frac{dx(t)}{dt} ,\:\:\mathrm{with}\:\: x(0)=x_0 $$

\vspace{0.2cm}

Which is the called ODE of characteristics lines, by solving it, one has 

$$ dx\left(t\right)=u\left(x,t\right)dt\:\Rightarrow \:x=u\left(x,t\right)t\:+x_0 ,\:\:\mathrm{with}\:\: u(x,t)=u_0(x_0)$$

Therefore, the characteristic equation is given by

$$ x = x_0 + u_0(x_0) \cdot t $$

\vspace{0.2cm}

By applying the given initial conditions

\vspace{0.2cm}

$$ x = \begin{cases}x_0 + t,\:\:\:\:\:\:&x_0<0,\\ x_0 + (1-x_0)\cdot t,&0\le x_0\le 1,\:x_0\:\in \mathbb{R},\\ 0,\:\:\:\:\:\:&x\ge 1.\end{cases} $$

\vspace{0.2cm}

Additionally, the characteristics are shown in the figure bellow.

\vspace{0.4cm}

\begin{center}
    
\begin{tikzpicture}[declare function={ft=0.1;
xl(\t)=0.5*\t-ft*sqrt(\t);xr(\t)=2+0.5*\t+ft*sqrt(2*\t);}]
\draw[-stealth] (-1,0) -- (5,0);
\draw[-stealth] (0,0) -- (0,4);
\draw[red,semithick,name path=pl] plot[variable=\t,domain=0:4,smooth] ({xl(\t)},{\t});
\draw[red,semithick,name path=pr] plot[variable=\t,domain=0:4,smooth] ({xr(\t)},{\t});

\begin{scope}[on background layer]
\foreach \X in {-1,-0.8,...,-0.2}
   {\path[name path=l\X] (\X,0) -- ++ (4,4);
\draw[blue,name intersections={of=pl and l\X}] (\X,0) 
   -- (intersection-1) -- (0,0-|intersection-1);}
\begin{scope} 
\clip  plot[variable=\t,domain=0:4,smooth] ({xl(\t)},{\t}) -| (-1,0);
\foreach \X in {-4,-3.8,...,-1.2}
    {\draw[blue] (\X,0) -- ++ (4,4);}
\end{scope}
\foreach \X in {2.2,2.4,...,4}
   {\path[name path=r\X] (\X,0) -- ++ (0,4);
\draw[blue,name intersections={of=pr and r\X}] (\X,0) 
   -- (intersection-1) -- (1,0);}
\end{scope}

\draw (5.2,0) node [anchor=north west][inner sep=0.75pt]    {$x$};
% Text Node
\draw (-0.1,4.4) node [anchor=north west][inner sep=0.75pt]    {$t$};

%\draw (1,1) node [anchor=north west][inner sep=0.75pt]    {$t_s = 1$};

\draw (1,-0.3) node [anchor=north west][inner sep=0.75pt]    {$t_s=1$};

 
\end{tikzpicture}

\end{center}


\vspace{0.4cm}


\subsection{Solution b)}
\textbf{(b)} Considering the following formula for the breaking time of the characteristics 

$$ t_s=-\frac{1}{\min _{x\in \mathbb{R}}\left(u_0'\left(x\right)\right)} $$

\vspace{0.2cm}

Considering again the given initial conditions ($u_0(x)$) and the $\min$ value, one simply computes the following

$$ t_s=-\frac{1}{\frac{d}{dx}\left(1-x\right)}=-\frac{1}{-1}=1 $$

\vspace{0.2cm}

And by observing at the Figure from item (a), the first shock (where the characteristic line intersect with each other) happens at time $t_s = 1$. 

\vspace{0.4cm}

\subsection{Solution c)}
\textbf{(c)} Considering that the solution to the inviscid Burger's  equation for the time $t < t_s = 1$ is 
$$ u(x,t) = u_0 (x-ut) $$

\vspace{0.1cm}

Where the argument $\xi \mapsto u_0 (\xi)$ is simply the function described in the given initial conditions (presented in the problem description), $ u(x,0) = u_0 (x) = u_0 (\xi)$.

Therefore, the classical solution is obtained by substituting the argument ($x - ut$) 

$$ u\left(x,t\right) = \begin{cases}1,\:\:\:\:\:\:&x<t,\\ 1-(x-ut),&t\le x\le 1,\:x\:\in \mathbb{R},\\ 0,\:\:\:\:\:\:&x\ge 1.\end{cases} $$

\vspace{0.1cm}

One must consider the following limit conditions for the above equation

$$I: x < 0  \Longleftrightarrow x - ut < 0 \Longleftrightarrow  x < ut \Longleftrightarrow x < t  $$

$$ II: 0 \le x < 1 \Longleftrightarrow  \begin{cases}x-ut\:<1&\\ x-ut\:\ge 0&\end{cases}  \Longleftrightarrow  \begin{cases}x\:<1 + (1-(x-ut))\cdot t&\\ x\:\ge (1-(x-ut))\cdot t&\end{cases} \Longleftrightarrow  t \le x < 1$$

$$III: x \ge 1  \Longleftrightarrow x - ut \ge 1 \Longleftrightarrow  x \ge 1+ ut \Longleftrightarrow x \ge 1  $$

\vspace{0.1cm}

Thus, the classical solution is 
\vspace{0.1cm}

$$ u\left(x,t\right) = \begin{cases}1,\:\:\:\:\:\:&x<t,\\ \dfrac{1-x}{1-t},&t\le x\le 1,\:x\:\in \mathbb{R},\\ 0,\:\:\:\:\:\:&x\ge 1.\end{cases} $$


\vspace{0.1cm}
for $0 \le t < 1$.
