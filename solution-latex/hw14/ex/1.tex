\section{Rankine-Hugoniot condition for the Euler equations}

The Euler equations can be used to model invicid fluid behaviour under certain
restrictions for external force and heat transfer.

\begin{align*}
    & \partial_{t}{\rho} + \partial_{x}(\rho v) = 0 \\
    & \partial_{t}{\rho v} + \partial_{x}(\rho v^{2} + p) = 0 \\
    & \partial_{t}{E} + \partial_{x}((E + p)v) = 0
\end{align*}

Where $(\rho, \rho v, E = \frac{1}{\gamma - 1}p + \frac{1}{2}\rho v^{2})$.

If such system develops a discontinuity over a boundary $\Gamma_{s} = x_{s}(t)$,
then the solution would need to respect the Rankine-Hugeniot jump condition.


\begin{align*}
    & s(\rho_{0} - \rho_{1}) = 
        \rho_{0}v_{0} - \rho_{1}v_{1}  \\
    & s(\rho_{0}v_{0} - \rho_{1}v_{1}) = 
        (\rho_{0}v^{2}_{0} + p_{0}) - (\rho_{1}v^{2}_{1} + p_{1}) \\
    & s(E_{0} - E_{1}) = 
        (E_{0} + p_{0})v_{0} - (E_{1} + p_{1})v_{1}
\end{align*}

Where $(\rho, \rho v, E = \frac{1}{\gamma - 1}p + \frac{1}{2}\rho v^{2})_{0,1}$,
are values of the physical properties approaching the discontinuity from left and
right respectively. 

From the mass conservation part of the Euler system of equations a relation between
densities can be derived:

\begin{align*}
    & s(\rho_{0} - \rho_{1}) = 
        \rho_{0}v_{0} - \rho_{1}v_{1}  \\
    & \rho_{0}(s - v_{0}) = \rho_{1}(s - v_{1}) \rightarrow \\
    & \frac{\rho_{1}}{\rho_{0}} = \frac{(s - v_{0})}{(s - v_{1})} \\
    & \frac{\rho_{1}}{\rho_{0}} = \frac{(M c_{0} - v_{0})}{(M c_{1} - v_{1})} \\
\end{align*}

However, a more classical verision of the above equation would be obtained if
the frame of reference is attached to the moving shock $s$:

\begin{align*}
    \frac{\rho_{1}}{\rho_{0}} = \frac{v_{0}}{v_{1}}
\end{align*}

Further analysis can be done in the frame of reference of the shockwave.
Second equation, as a conservation of momentum can be rewritten as follows:

\begin{align*}
        \frac{\rho^{2}_{0}v^{2}_{0}}{\rho_{0}} + p_{0} 
            = \frac{\rho^{2}_{1}v^{2}_{1}}{\rho_{1}} + p_{1}
\end{align*}

From the conservation of mass it may be observed that 
$\rho^{2}_{1}v^{2}_{1} = \rho^{2}_{0}v^{2}_{0} = a^{2}$

\begin{align*}
        & \frac{a^{2}}{\rho_{0}} + p_{0} 
            = \frac{a^{2}}{\rho_{1}} + p_{1} \\
        & a^{2} = \frac{p_{1} - p_{0}}{\frac{1}{\rho_{1}} - \frac{0}{\rho_{0}}} \\
\end{align*}

The conservation of energy may also examined from the perspective of a standing shock
with incoming and outgoing flows to the left and right:

\begin{align*}
        & (E_{0} + p_{0})v_{0} = (E_{1} + p_{1})v_{1} \\
        & (\frac{1}{\gamma - 1}p_{0} + \frac{1}{2}\rho_{0} v^{2}_{0} + p_{0})v_{0} 
        = (\frac{1}{\gamma - 1}p_{1} + \frac{1}{2}\rho_{1} v^{2}_{1} + p_{1})v_{1} \\
        & (\frac{1}{\gamma - 1}\frac{p_{0}}{\rho_{0}} + \frac{1}{2} v^{2}_{0} + \frac{p_{0}}{\rho_{0}})\rho_{0}v_{0} 
        = (\frac{1}{\gamma - 1}\frac{p_{1}}{\rho_{1}} + \frac{1}{2} v^{2}_{1} + \frac{p_{1}}{\rho_{1}})\rho_{1}v_{1} \\
        & (\frac{\gamma}{\gamma - 1}\frac{p_{0}}{\rho_{0}} + \frac{1}{2} v^{2}_{0})\rho_{0}v_{0} 
        = (\frac{\gamma}{\gamma - 1}\frac{p_{1}}{\rho_{1}} + \frac{1}{2} v^{2}_{1})\rho_{1}v_{1} \\
        & (\frac{c^{2}_{0}}{\gamma - 1} + \frac{1}{2} v^{2}_{0})\rho_{0}v_{0} 
        = (\frac{c^{2}_{1}}{\gamma - 1} + \frac{1}{2} v^{2}_{1})\rho_{1}v_{1} \\
        & \frac{c^{2}_{0}}{\gamma - 1} + \frac{1}{2} v^{2}_{0}
        = \frac{c^{2}_{1}}{\gamma - 1} + \frac{1}{2} v^{2}_{1} \\
\end{align*}

The above equation may also be rewritten in terms $a^{2}$ quantity intriduced earlier:

\begin{align*}
        & \frac{\gamma}{\gamma - 1}\frac{p_{0}}{\rho_{0}} + \frac{a^{2}}{2\rho^{2}_{0}}
        = \frac{\gamma}{\gamma - 1}\frac{p_{1}}{\rho_{1}} + \frac{a^{2}}{2\rho^{2}_{1}} \\
        & \frac{a^{2}}{2} \left[ \frac{1}{\rho^{2}_{0}} - \frac{1}{\rho^{2}_{1}}\right]
        =  \frac{\gamma}{\gamma - 1} \left[ \frac{p_{1}}{\rho_{1}} - \frac{p_{0}}{\rho_{0}} \right]\\
        & \frac{1}{2}(p_{1} - p_{0}) \left[ \frac{1}{\rho_{0}} - \frac{1}{\rho_{1}}\right]
        =  \frac{\gamma}{\gamma - 1} \left[ \frac{p_{1}}{\rho_{1}} - \frac{p_{0}}{\rho_{0}} \right]\\
        & \frac{1}{\rho_{0}} \left[\frac{\gamma + 1}{\gamma - 1}p_{0} + p_{1} \right]
        = \frac{1}{\rho_{1}} \left[\frac{\gamma + 1}{\gamma - 1}p_{1} + p_{0} \right]\\
\end{align*}

This results in the following relation:

\begin{align*}
        & \frac{\rho_{1}}{\rho_{0}} 
        = \frac{(\gamma + 1)p_{1} + (\gamma - 1)p_{0}}{(\gamma + 1)p_{0} + (\gamma - 1)p_{1} } \\
\end{align*}

Which from conservation of mass is equal to:

\begin{align*}
        & \frac{v_{0}}{v_{1}} 
        = \frac{(\gamma + 1)p_{1} + (\gamma - 1)p_{0}}{(\gamma + 1)p_{0} + (\gamma - 1)p_{1}} \rightarrow \\
        & \frac{p_{1}}{p_{0}} 
        = \frac{(\gamma + 1)v_{0} - (\gamma - 1)v_{1}}{(\gamma + 1)v_{1} - (\gamma - 1)v_{0}} \rightarrow \\
        & \frac{p_{1}}{p_{0}} 
        = \frac{(\gamma + 1)(s - v_{0}) - (\gamma - 1)(s - v_{1})}{(\gamma + 1)(s - v_{1}) - (\gamma - 1)(s - v_{0})} \rightarrow \\
        & \frac{p_{1}}{p_{0}} 
        = \frac{(\gamma + 1)(M c_{0} - v_{0}) - (\gamma - 1)(M c_{1} - v_{1})}{(\gamma + 1)(M c_{1} - v_{1}) - (\gamma - 1)(M c_{0} - v_{0})} \\
\end{align*}