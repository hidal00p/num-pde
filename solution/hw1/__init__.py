from typing import Tuple
from solution import Solution
import numpy as np
import matplotlib.pyplot as plt

'''
The following problem is posed:
-∆u = 1 on [0,1]^2
 un = 1 on Г_{1}
  u = 0 on Г_{0}

       _x1_ ^
            1     Г_{0}
            | x x x x x x x x
            | x x x x x x x x
      Г_{0} | x x x x x x x x Г_{1}
            | x x x x x x x x
            | x x x x x x x x
            | x x x x x x x x
            0- - - - - - - - - 1>
                  Г_{0}          _x0_

Г_{0} - Dirichlet
Г_{1} - Neumann
Points on the intesection of Г_{0} with Г_{1} are treated as parts of Г_{0} because central difference discretization yields one extra unknown for a ghost point
in the _x1_ direction (to aid this a forward and backward difference may be applied)
'''

class HW1(Solution):
    '''
    _x1_ is equivalent to _x_ direction in the Cartesian CS
    _x2_ is orthogonal to _x1_ (_y_ direction)

    L - domain length in _x1_
    H - domain height in _x2_
    n - the number of internal points in the _x1_ direction
    m - the number of internal points in the _x2_ direction
    '''
    def __init__(self, L:float = 1.0, H:float = 1.0, n:int = 100, m:int = 100, fDebug: bool = True):
        self.fDebug = fDebug
        self.L = L
        self.H = H
        self.n = n
        self.m = m

        self.h1 = L/(n+1) # interval length in _x1_
        self.h2 = H/(m+1) # interval length in _x2_
        
        self.N = n+2 # total number of discretization nodes (with the boudanry) in _x1_
        self.M = m+2 # total number of discretization nodes (with the boudanry) in _x2_
        
        if self.fDebug:
            np.set_printoptions(linewidth=200)

        self.dim = (self.N*self.M, self.N*self.M)
        self.A = np.zeros(self.dim, dtype=np.float32)
        self.coef1 = (1/self.h1)**2
        self.coef2 = (1/self.h2)**2

        self.F = np.ones((self.N*self.M, 1), dtype=np.float32) # source vector
        self.G = np.zeros((self.N*self.M, 1), dtype=np.float32) # boundary condition vector
        
        assert self.A.shape == self.dim, f'Dimension is set incorrectly.\nExpected: {self.dim}\nObtained: {self.A.shape}'
        self._initProblem()
    
    def _getFiniteDifferenceRow(self, index):
            u = np.zeros(self.N*self.M, dtype=np.float32)
            u[index] = 2*(self.coef1+self.coef2)
            u[index+1] = -self.coef1
            u[index-1] = -self.coef1
            u[index+self.N] = -self.coef2
            u[index-self.N] = -self.coef2
            return u
    
    '''
    Checks if point is on the boudanry and if yes is boudanry Dirrichlet
    -> (fBoudary, fDirrichlet)
    '''
    def _checkBoundary(self, point: Tuple[int, int]) -> Tuple[bool, bool] :
        x, y = point
        fBoudanry = (x == 0 or y == 0 or x == self.N-1 or y == self.M-1)
        if not fBoudanry:
            return False, False
        
        fDirrichlet = x == 0 or y == 0 or y == self.M-1
        return True, fDirrichlet

    def _initProblem(self):
        # internal subdomain initialization
        for j in range(0, self.M):
            for i in range(0, self.N):
                index = i + j*self.N
                # check if we are at the boundary
                fBoudanry, fDirrichlet = self._checkBoundary((i, j))
                if(fBoudanry):
                    # apply boundary conditions
                    u = np.zeros(self.N*self.M, dtype=np.float32)
                    if(fDirrichlet):
                        # we at Г_{0}
                        u[index] = 1
                        self.F[index, 0] = 0
                    else:
                        # we at Г_{1}
                        u[index] = 2*(self.coef1+self.coef2)
                        u[index-1] = -2*self.coef1
                        u[index+self.N] = -self.coef2
                        u[index-self.N] = -self.coef2
                        self.G[index] = 2 / self.h1
                    self.A[index] = u
                    continue
                self.A[index] = self._getFiniteDifferenceRow(index)
        if self.fDebug:
            print(
                f'{self.A}\n'
                f'{self.F}\n'
                f'{self.G}'
            )
    
    def plot(self):
        X, Y = [i*self.h1 for i in range(0, self.N)], [i*self.h2 for i in range(0, self.M)]
        fig, ax = plt.subplots()
        contf = ax.contourf(X, Y, self.u)
        
        fig.colorbar(contf, ax=ax)
        ax.set_title('Solution to 2D Poisson problem')
        ax.set_xlabel('Spatial horizontal direction')
        ax.set_ylabel('Spatial vertical direction')
        
        plt.show()
    
    def solve(self):
        self.u: np.ndarray = np.round(np.linalg.solve(self.A, self.F+self.G), decimals=6).reshape((self.N, self.M))
        if self.fDebug:
            print(
                f'Solutions:\n'
                f'{self.u}'
            )
            self.plot()
