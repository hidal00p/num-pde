'''
A simple type contract, which defines an interface for an
individual solution.

The objective is to provide a small framework, within which writing consequtive solutions
does not result in a complete mess of the codebase.
'''

class Solution:
    def solve(self):
        raise 'Must be implemented by the inherited class'